<?php

/**
 * Drush command to clear cache_appreciation_engine_api.
 *
 * @return array
 */
function appreciation_engine_api_drush_command() {
  $commands = array();

  // ### Cache Clear Features
  $commands['appreciation_engine_api_cc'] = array(
    'description' => 'Clear the cache of appreciation engine api requests.',
    'callback' => '_appreciation_engine_api_drush_cache_clear',
    'arguments' => array(),
    'options' => array(),
    'aliases' => array('cc-ae-api'),
  );

  return $commands;
}

function _appreciation_engine_api_drush_cache_clear(){
  appreciation_engine_api_cache_clear('*', TRUE);
}
<?php

namespace Appreciation_Engine;
use \stdClass;

//# Appreciation Engine API Interface
class API {
  private static $API_BASE = 'https://theappreciationengine.com/api/';
  private static $api_key = NULL;

  private $settings;

  //## Constructor
  public function __construct($options = array()){

    $ae_domain = variable_get('ae_domain', 'https://theappreciationengine.com');
    self::$API_BASE = "$ae_domain/api/";

    //Set api_key
    if(!isset(self::$api_key)){
      self::$api_key = appreciation_engine_api_key();
      if(!isset(self::$api_key)){
        drupal_set_message(t('Appreciation Engine API key not set.'), 'error', FALSE);
      }
    }

    $this->settings = new stdClass();
    $ae_api_settings = variable_get('appreciation_engine_api_settings');
    $settings = empty($ae_api_settings) ? array() : $ae_api_settings;
    //Merge options into API settings
    $settings = drupal_array_merge_deep($settings, $options);
    foreach( Endpoints::$options as $endpoint => $options){
      $data = new stdClass();
      $settings[$endpoint] = isset($settings[$endpoint]) ? $settings[$endpoint]: array();
      $data->options = $options;
      //allowed parameters
      $data->allowed_parameters = isset(Endpoints::$parameters[$endpoint]) ? Endpoints::$parameters[$endpoint] : array();
      if( isset($options['cache']) && $options['cache'] == TRUE ){
        $data->cacheDuration = isset($settings[$endpoint]['cacheDuration']) ? intval($settings[$endpoint]['cacheDuration']) : 300;
        $data->bufferSize = isset($settings[$endpoint]['bufferSize']) ? intval($settings[$endpoint]['bufferSize']) : 100;
      }else{
        $data->cacheDuration = 0;
      }

      if($endpoint === Endpoints::MEMBER){
        $data->refreshInterval = isset($settings[$endpoint]['userRefreshInterval']) ? intval($settings[$endpoint]['userRefreshInterval']) : 500;
      }
      $this->settings->{$endpoint} = $data;
    }
    $this->use_cache = variable_get('appreciation_engine_api_cache', '1') == '1';
  }

  //## Getter
  public function __get($property_name) {
    if ($property_name === 'API_KEY' ) {
      //### API Key
      return self::$api_key;
    }
    return NULL;
  }

  //##API Requests
  //### Members
  public function members( array $parameters = array()){
    return $this->getBufferedData($parameters, Endpoints::MEMBERS);
  }

  //### Feed
  public function feed( array $parameters = array() ){
    return $this->getBufferedData($parameters, Endpoints::FEED);
  }

  //### User
  public function user( array $parameters = array(), $CACHE = TRUE){
    return self::member($parameters, $CACHE);
  }

  //### Member
  public function member( array $parameters = array(), $CACHE = TRUE){
    $timestamp = $_SERVER['REQUEST_TIME'];
    if(isset($parameters['refresh']) && $parameters['refresh'] == 1){
      //The refresh parameter triggers AE user update.
      $cache_key = $this->getCacheKey($parameters, Endpoints::MEMBER);
      $cache_value = cache_get("{$cache_key}_refresh", 'cache_appreciation_engine_api');
      if($cache_value === FALSE || $timestamp > $cache_value->expire){
        //If there is no previous refresh attempt or it's been long enough then bypass caching.
        $result = $this->getData($parameters, Endpoints::MEMBER, FALSE);
        $result->refresh = TRUE;
        $result->time = $this->settings->user->refreshInterval;
        if(isset($result->data)){
          cache_set($cache_key, $result, 'cache_appreciation_engine_api', $timestamp + $this->settings->user->cacheDuration);
        }
        cache_set("{$cache_key}_refresh", $result, 'cache_appreciation_engine_api', $timestamp + $this->settings->user->refreshInterval);
        return $result;
      }else{
        if(is_object($cache_value->data)){
          $result = $cache_value->data;
        }else{
          $result = new stdClass();// $cache_value->data
        }
        $result->refresh = FALSE;
        $result->time = $cache_value->expire - $timestamp;
        return $result;
      }
    }
    return $this->getData($parameters, Endpoints::MEMBER, $CACHE);
  }

  //### Info
  public function info( array $parameters = array() ){
    return $this->getData($parameters, Endpoints::INFO);
  }

  //### Check
  public function check( array $parameters = array() ){
    return self::getData($parameters, Endpoints::CHECK);
  }

  //### Opt In
  public function opt_in( array $parameters = array() ){
    //Note that these requests aren't cached.
    return self::request($parameters, Endpoints::OPT_IN, 'POST');
  }

  //### Opt Out
  public function opt_out( array $parameters = array() ){
    return self::request($parameters, Endpoints::OPT_OUT, 'POST');
  }

  //### Record
  public function record( array $parameters = array() ){
    return self::request($parameters, Endpoints::RECORD, 'POST');
  }

  //### Member Verify
  public function member_verify( array $parameters = array()){
    return self::request($parameters, Endpoints::MEMBER_VERIFY, 'POST');
  }

  //### Member Update
  public function member_update( array $parameters = array()){
    return self::request($parameters, Endpoints::MEMBER_UPDATE, 'POST');
  }

  //### Member Register
  public function member_register( array $parameters = array()){
    return self::request($parameters, Endpoints::MEMBER_REGISTER, 'POST');
  }

  //### Member Register
  public function member_refresh_tokens( array $parameters = array()){
    return self::request($parameters, Endpoints::MEMBER_REFRESH_TOKENS, 'POST');
  }

  //### Reset Password
  public function reset_password( array $parameters = array()){
    return self::request($parameters, Endpoints::RESET_PASSWORD, 'POST');
  }

  //### Auth
  public function auth( array $parameters = array(), $CACHE = FALSE){
    return self::request($parameters, Endpoints::AUTH, 'POST', $CACHE);
  }

  //## Get API data from cache
  private function getData(array $parameters = array(), $endpoint, $CACHE = TRUE){
    // Load the data from cache
    if($CACHE && $this->settings->{$endpoint}->cacheDuration != 0 && $cache_data = $this->cacheGet($parameters, $endpoint)){
      return $cache_data;
    }
    // or make the API request.
    $response = self::request($parameters, $endpoint, 'GET', $CACHE);
    if($CACHE && !isset($response->error)){
      //Don't cache API errors
      $this->cacheSave($parameters, $endpoint, $response);
    }
    return $response;
  }

  private function getBufferedData(array $parameters = array(), $endpoint){
    //Get the offset and limit as #s
    $limit_params = self::parseLimit($parameters, $endpoint);
    $offset = $limit_params['offset'];
    $limit = $limit_params['limit'];

    //Map this request to a locally cached page.
    $buffered_pages = $this->getBufferedPages($endpoint, $offset, $limit);
    $buffered_data = array();
    foreach($buffered_pages as $buffered_offset => $buffered_page){
      //Alter the parameters to request a buffered page
      $buffered_parameters = $parameters;
      $buffered_parameters['limit'] = self::createLimit($buffered_page['offset'], $buffered_page['limit']);
      if($cache_data = $this->cacheGet($buffered_parameters, $endpoint)){
        // and load the data from cache
        $buffered_data[$buffered_offset] = $cache_data;
        continue;
      }
      // or make the API request.
      $response = self::request($buffered_parameters, $endpoint);
      $buffered_data[$buffered_offset] = $response;
      if(!isset($response->error)){
        //Don't cache API errors
        $this->cacheSave($buffered_parameters, $endpoint, $response);
      }
    }
    //Finally, pull the data requested from the buffered pages.
    return self::pageBufferedData($buffered_data, $offset, $limit);
  }

  //### Buffered paging helper
  private static function pageBufferedData( &$buffered_data, $offset, $limit){
    //Get the requested data from across the pages we've pulled.
    $result = NULL;
    foreach($buffered_data as $buffered_offset => $data){
      if($result === NULL){
        //From the first page of data
        $start = $offset - $buffered_offset;
        $end = $start + $limit;
        if(isset($data->items)){
          $num_items = count($data->items);
          //Remove extra items from before the offset
          if($start > 0){
            array_splice($data->items, 0, $start);
          }
          // and after the limit.
          if($end < $num_items){
            array_splice($data->items, $end - $start, $num_items - $end);
          }
        }
        $result = $data;
      }else{
        //If this request crosses over to another page then add items from other pages.
        $end = $limit - ($buffered_offset - $offset);
        if(isset($data->items)){
          $num_items = count($data->items);
          $length = min($end, $num_items);
          $items = array_slice($data->items, 0, $length);
          array_splice($result->items, -1, 0, $items);
        }
      }
    }
    //Update count of total items count($result->items)
    if(isset($result->totalSize)){
      $result->totalSize = count($result->items);
    }
    return $result;
  }
  
  //## Caching
  private function cacheGet(array $parameters, $endpoint){
    if(!$this->use_cache){ return FALSE; }
    try{
      $cache_key = $this->getCacheKey($parameters, $endpoint);
      $cache_value = cache_get($cache_key, 'cache_appreciation_engine_api');
      if($cache_value !== FALSE){
        // If a cache hit
        if( $_SERVER['REQUEST_TIME'] < $cache_value->expire){
          // and the cache isn't expired
          $cache_data = $cache_value->data;
          $cache_data->cache = TRUE;
          $cache_data->expires = $cache_value->expire;
          // then return the cached data;
          return $cache_data;
        }else{
          // otherwise, clear the cache for every page with these parameters.
          $this->cacheExpire($parameters, $endpoint);
        }
      }
    }catch(Exception $e){
      watchdog('appreciation_engine_api', $e->getMessage(), NULL, WATCHDOG_ERROR);
    }
    return FALSE;
  }

  private function cacheSave(array $parameters, $endpoint, $data){
    if(!$this->use_cache){ return; }
    try{
      $cache_duration = $this->settings->{$endpoint}->cacheDuration;
      $cache_key = $this->getCacheKey($parameters, $endpoint);

      if(!empty($this->settings->{$endpoint}->options['buffer'])){
        //For all paged requests get the page index
        $index_cache_key = $this->getCacheKey($parameters, $endpoint, TRUE);
        $index_cache = cache_get($index_cache_key, 'cache_appreciation_engine_api');
        if($index_cache !== FALSE){
          $index = $index_cache->data;
        }else{
          // or build a new index
          $index = array();
        }
        $index[] = $cache_key;
        //and save it to the index cache key.
        cache_set($index_cache_key, $index, 'cache_appreciation_engine_api', CACHE_PERMANENT);
      }
      // save the data in cache for the specified time.
      cache_set($cache_key, $data, 'cache_appreciation_engine_api', $_SERVER['REQUEST_TIME'] + $cache_duration);
    }catch(Exception $e){
      watchdog('appreciation_engine_api', $e->getMessage(), NULL, WATCHDOG_ERROR);
    }
  }

  private function cacheExpire(array $parameters, $endpoint){
    if(!$this->use_cache){ return; }
    try{
      if(!empty($this->settings->{$endpoint}->options['buffer'])){
        //Get the index cache value
        $index_cache_key = $this->getCacheKey($parameters, $endpoint, TRUE);
        $index_cache = cache_get($index_cache_key, 'cache_appreciation_engine_api');
        if($index_cache !== FALSE && !empty($index_cache->data)){
          //We've got the index
          $index = $index_cache->data;
          //now clear the index and all referenced cache keys.
          $index[] = $index_cache_key;
          cache_clear_all($index, 'cache_appreciation_engine_api');
        }else{
          //If the index isn't found for some reason then clear cache by pattern.
          $parts = explode('_', $index_cache_key);
          if(count($parts) >= 2){
            $cache_pattern = "{$parts[0]}_{$parts[1]}*";
            cache_clear_all($cache_pattern, 'cache_appreciation_engine_api', TRUE);
          }
        }
      }else{
        //Otherwise, it should already be expired don't clear the cache.
      }
    }catch(Exception $e){
      watchdog('appreciation_engine_api', $e->getMessage(), NULL, WATCHDOG_ERROR);
    }
  }

  private function getCacheKey(array $parameters, $endpoint, $index = FALSE){
    $cache_key = '';

    if(!empty($this->settings->{$endpoint}->options['buffer'])){
      //Endpoints that locally buffer data store responses in multiple pages;
      Endpoints::$defaults[$endpoint]['limit'];
      if(!empty($parameters['limit'])){
        // each page needs a limit,offset to identify it.
        $limit = self::parseLimit($parameters, $endpoint);
        $offset = $limit['offset'];
        $limit = $limit['limit'];
        $bufferSize = $this->settings->{$endpoint}->bufferSize;
        $offset = floor($offset / ($bufferSize - 1)) * $bufferSize;
        //The limit paging parameter must not be part of the cache key so pages have the same hash value.
        unset($parameters['limit']);
      }else{
        $limit = Endpoints::$defaults[$endpoint]['limit']['default'];
        $offset = 0;
      }
    }

    if( isset(Endpoints::$parameters[$endpoint])){
      //We must enforce an explicit order for the cache keys to maintain hash.
      foreach(Endpoints::$parameters[$endpoint] as $param){
        if(!empty($parameters[$param])){
          //TODO check for ignored cache key parameters
          if($param == 'refresh'){ continue; }
          $cache_key .= $parameters[$param];
        }
      }
    }
    //@see http://php.net/manual/en/function.hash.php
    $cache_key = "{$endpoint}_".hash('sha256', $cache_key);// previously base64_encode

    if(!empty($this->settings->{$endpoint}->options['buffer'])){
      if($index === FALSE){
        //Add the limit page index to the cache key.
        $cache_key .= "_{$offset}_{$limit}";
      }else{
        //Buffered requests also use an index, a listing of all the associated page cache keys.
        $cache_key .= '_index';
      }
    }

    return $cache_key;
  }

  //## Make the API Request
  private static function request(array $parameters = array(), $endpoint, $http_method = 'GET', $CACHE = TRUE){
    if(empty(self::$api_key)){
      return array('error' => array('message' => 'Api key not set.'));
    }

    $allowed_params = Endpoints::$parameters[$endpoint];
    $allowed_params[] = 't';
    //limit allowed parameters
    $parameters = array_intersect_key($parameters, drupal_map_assoc($allowed_params));
    $parameters['apiKey'] = self::$api_key;
    $options = array(
      'headers' => array(
        'Accept' => 'application/json',
      ),
      'method'  => $http_method,
    );

    if($CACHE == FALSE){
      $parameters['t'] = time();
      if(isset($parameters['refresh']) && $parameters['refresh'] == 1){
        $options['timeout'] = 60;
      }
    }else{
      $ae_settings = variable_get('appreciation_engine_api_settings');
      $endpoint_settings = isset($ae_settings[$endpoint]) ? $ae_settings[$endpoint]: array();
      $cacheDuration = isset($endpoint_settings['cacheDuration']) ? intval($endpoint_settings['cacheDuration']) : NULL;
      if(isset($cacheDuration) && $cacheDuration > 0){
        $parameters['t'] = intval((intval(date('i')) * 60 + intval(date('s'))) / $cacheDuration);
      }
    }

    $data = http_build_query($parameters, '', '&');
    $query = '?' . $data;

    try{
      $request_url = self::$API_BASE . $endpoint . $query;
      $response = drupal_http_request($request_url, $options);
      $result = isset($response->data) ? json_decode($response->data) : NULL;
      if(!isset($result) || isset($result->error)){
        $message = 'An unspecified api error occurred.';
        if(!isset($result)){ $result = new stdClass(); }
        if(isset($result->error->message)){
          $message = $result->error->message;
        }else if(isset($response->error)){
          $message = $response->error;
          $result->error = new stdClass();
          $result->error->message = $message;
        }
        watchdog('appreciation_engine_api', '%message | url :: %url ' . $request_url, array('%message' => $message, '%url' => $request_url), WATCHDOG_ERROR);
      }
    }catch(Exception $e){
      watchdog('appreciation_engine_api', $e->getMessage(), NULL, WATCHDOG_CRITICAL);
      return new stdClass();
    }

    return $result;
  }

  //## Miscellaneous Utilities
  public static function parseLimit($parameters, $endpoint){
    if(isset($parameters['limit'])){
      $limit = explode(',', $parameters['limit']);
      if(count($limit) > 1){
        $offset = intval($limit[0]);
        $limit = intval($limit[1]);
      }else if(count($limit) == 1){
        $limit = intval($limit[0]);
        $offset = 0;
      }
    }else{
      //Not set, get the API default
      $offset = 0;
      $limit = isset(Endpoints::$defaults[$endpoint]) ? intval(Endpoints::$defaults[$endpoint]['limit']['default']) : 10;
    }
    return array( 'offset' => $offset, 'limit' => $limit );
  }

  public static function createLimit($offset, $limit){
    $limit_param = '';
    //Create the limit filter string
    if($offset === 0){
      $limit_param .= $limit;
    }else{
      $limit_param .= $offset.','.+$limit;
    }
    return $limit_param;
  }

  private function getBufferedPages($endpoint, $offset, $limit){
    //Get an array of the pages this offset + limit spans
    $bufferSize = $this->settings->{$endpoint}->bufferSize;
    $pages = array();
    $start = floor($offset / $bufferSize) * $bufferSize;
    for($bufferOffset = $start; $bufferOffset < $offset + $limit; $bufferOffset += $bufferSize){
      $pages[$bufferOffset] = array( 'offset' => $bufferOffset, 'limit' => $bufferSize );
    }
    return $pages;
  }
  
}

//# Endpoint
/**
 * AE Endpoint List
 * @see https://developer.appreciationengine.com/documentation/api-documentation
 */
class Endpoints {
  const AUTH = 'auth';
  const BRAND_LIST = 'brandlist';
  const CHECK = 'check';
  const FEED = 'feed';
  const INFO = 'info';
  const LOGINS = 'logins'; // TODO
  const OPT_IN = 'optin'; // POST
  const OPT_OUT = 'optout'; // POST
  const RECORD = 'record'; // POST
  const USER = 'user';
  const MEMBERS = 'members';
  const MEMBER_REGISTER = 'member_register'; // POST
  const MEMBER_UPDATE = 'member_update'; // POST
  const MEMBER = 'member';
  const MEMBER_VERIFY = 'member_verify'; // POST
  const RESET_PASSWORD = 'reset_password'; // POST
  const MEMBER_REFRESH_TOKENS = 'member_refresh_tokens'; // POSt

  /**
   * @var array supported parameters for each supported endpoint
   */
  static $parameters = array(
    Endpoints::CHECK => array( 'activityID', 'brandID', 'id', 'partnerCode', 'partnerID' ),
    Endpoints::FEED => array( 'brandID', 'segment', 'includeOneTime', 'country', 'region', 'services', 'keyword',
      'period', 'period_end', 'start_date', 'end_date', 'order_by_recorded', 'members', 'exclude_members', 'optedin',
      'all_member_actions', 'limit' ),
    Endpoints::INFO => array( 'brandID' ),
    Endpoints::AUTH => array( 'accessToken', 'details' ),
    Endpoints::OPT_IN => array( 'segment', 'id', 'email', 'partnerCode', 'partnerID' ),
    Endpoints::OPT_OUT => array( 'segment', 'id', 'email', 'partnerCode', 'partnerID' ),
    Endpoints::RECORD => array( 'activityID', 'id', 'partnerCode', 'partnerID', 'content', 'link', 'marker', 'timestamp' ),
    Endpoints::MEMBERS => array( 'brandID', 'segment', 'segment', 'activityID', 'includeOneTime', 'fields', 'country', 'region', 'services', 'period',
      'period_end', 'start_date', 'end_date', 'members', 'exclude_members', 'optedin', 'limit', 'order_by', 'sort' ),
    Endpoints::MEMBER => array( 'id', 'email', 'verified_email', 'username', 'service', 'partnerCode', 'partnerID', 'period',
      'period_end', 'start_date', 'end_date', 'refresh', 'threshold', 'brandID',
      'segment', 'activityID', 'includeOneTime', 'services', 'showposition', 'showbrands', 'showactions', 'limit', 'extended', 'optedin' ),
    Endpoints::AUTH => array( 'accessToken', 'details' ),
    Endpoints::MEMBER_REGISTER => array( 'service', 'authtoken', 'authsecret', 'uid', 'firstname', 'lastname', 'username',
      'email', 'password', 'dob', 'partner', 'code', 'sourceuid', 'domain', 'country', 'ip'  ),
    Endpoints::MEMBER_UPDATE => array( 'id', 'partnerCode', 'partnerID', 'firstname', 'surname', 'username', 'email',
      'birthdate', 'address', 'addressline2', 'city', 'state', 'country', 'postcode', 'homephone', 'mobilephone', 'website', 'bio', 'gender' ),
    Endpoints::MEMBER_VERIFY => array( 'service', 'verified_email', 'id' ),
    Endpoints::RESET_PASSWORD => array( 'email', 'password', 'member_service_id' ),
    Endpoints::MEMBER_REFRESH_TOKENS => array( 'serviceID' ),
  );

  /**
   * @var array API configuration information
   */
  static $options = array(
    Endpoints::FEED => array( 'cache' => TRUE, 'buffer'=>TRUE, 'method' => 'GET', ),
    Endpoints::INFO => array( 'cache' => TRUE, 'method' => 'GET', ),
    Endpoints::MEMBER => array( 'cache' => TRUE, 'authenticated' => TRUE, 'method' => 'GET', ),
    Endpoints::MEMBERS => array( 'cache' => TRUE, 'buffer' => TRUE, 'method' => 'GET', ),
    Endpoints::RECORD => array( 'authenticated' => TRUE, 'method' => 'POST', ),
    Endpoints::OPT_IN => array( 'authenticated' => TRUE, 'method' => 'POST', ),
    Endpoints::OPT_OUT => array( 'authenticated' => TRUE, 'method' => 'POST', ),
    Endpoints::CHECK => array( 'authenticated' => TRUE, 'cache' => TRUE, 'method' => 'GET', ),
    Endpoints::AUTH => array( 'method' => 'POST', ),
    Endpoints::MEMBER_REGISTER => array( 'method' => 'POST', ),
    Endpoints::MEMBER_UPDATE => array( 'method' => 'POST', ),
    Endpoints::MEMBER_VERIFY => array( 'method' => 'POST', ),
    Endpoints::RESET_PASSWORD => array( 'method' => 'POST', ),
    Endpoints::MEMBER_REFRESH_TOKENS => array( 'method' => 'POST', ),
  );

  /**
   * @var array default values used by api endpoints (not module defaults).
   */
  static $defaults = array(
    Endpoints::FEED => array( 'limit' => array( 'default' => 10 , 'max' => 1000), ),
    Endpoints::MEMBERS => array( 'limit' => array( 'default' => 10 , 'max' => 1000), ),
  );

}

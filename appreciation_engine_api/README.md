appreciation_engine_api
=======================

A Drupal module for Appreciation Engine API integration..

Appreciation Engine API requests are cached locally for a configurable period of time before they can be refreshed from the API.

[theappreciationengine.com](http://theappreciationengine.com "The Appreciation Engine")

<?php
/**
 * @file appreciation_engine_api.admin
 * @author Malcolm Poindexter <malcolm.poindexter@umusic.com>
 * Admin forms, settings, and permissions
 */

module_load_include('inc', 'appreciation_engine_api', 'appreciation_engine_api.class');
use Appreciation_Engine\Endpoints;

/**
 * Implements hook_permission().
 */
function appreciation_engine_api_permission() {
	$permissions['administer appreciation_engine_api settings'] = array(
		'title' => t('Administer Appreciation Engine API settings.'),
	);
	return $permissions;
}

/**
 * Access callback for the entity API.
 */
function appreciation_engine_api_admin_access($op, $entity = NULL, $account = NULL, $entity_type) {
	return user_access('administer appreciation_engine_api settings', $account);
}

/**
 * Implements hook_menu()
 */
function appreciation_engine_api_menu() {
	$items = array();

  $items['admin/config/people/appreciation_engine/api'] = array(
    'title' => 'API',
    'description' => 'Settings for appreciation_engine_api module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('appreciation_engine_api_admin'),
    'access arguments' => array('administer appreciation_engine_api settings'),
    'type' => MENU_LOCAL_TASK,
  );

	return $items;
}

/**
 * Implements hook_admin()
 * Defines configuration form for the Appreciation Engine Module.
 */
function appreciation_engine_api_admin() {
	$form = array();

  //### API Settings
  $ae_settings = variable_get('appreciation_engine_api_settings');
  $form['appreciation_engine_api_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Settings'),
    '#description' => t('<p class="description">For more details see the official <a href="https://developer.appreciationengine.com/documentation/api-documentation">rest api documentation</a>.</p>'),
    '#weight' => 2,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $time_options = array(
    '0' => t('None'),
    '15' => t('15 Seconds'),
    '30' => t('30 Seconds'),
    '60' => t('1 Minute'),
    '120' => t('2 Minutes'),
    '180' => t('3 Minutes'),
    '300' => t('5 Minutes'),
    '600' => t('10 Minutes'),
    '900' => t('15 Minutes'),
    '1800' => t('30 Minutes'),
    '2700' => t('45 Minutes'),
    '3600' => t('1 Hour'),
    //'7200' => t('2 Hours'),
    //'10800' => t('3 Hours'),
  );

  $weight = 0;
  $defaults =[];
  foreach( Endpoints::$options as $endpoint => $options){
    $element = array();
    $endpoint_name = ucwords(str_replace('_', ' ', $endpoint));
    $defaults = isset($ae_settings[$endpoint]) ? $ae_settings[$endpoint]: array();
    if($endpoint === Endpoints::MEMBER){ continue; }

    //### Endpoint Cache Settings
    if( isset($options['cache']) && $options['cache'] == TRUE ){
      $element['appreciation_engine_api_settings'][$endpoint]['cacheDuration'] = array(
        '#type' => 'select',
        '#title' => t('Cache Duration'),
        '#description' => t("Local site request cache time for the {$endpoint} endpoint [Cache By Parameter]. The AE API has a 5 minute CDN cache, but local caching improves response time, especially for expensive requests."),
        '#options' => $time_options,
        '#default_value' => isset($defaults['cacheDuration']) ? $defaults['cacheDuration'] : 300,
        '#weight' => 1,
      );

      if( isset($options['buffer']) && $options['buffer'] == TRUE ){
        $element['appreciation_engine_api_settings'][$endpoint]['bufferSize'] = array(
          '#type' => 'textfield',
          '#title' => t('Local Request Buffer'),
          '#description' => t('The api request limit / page size parameter (max 1000). For repeated requests a large buffer size will increase cache HITS with a longer request time. For frequent different requests, a smaller page size and shorter response time may be preferred.'),
          '#default_value' => isset($defaults['bufferSize']) ? $defaults['bufferSize'] : 100,
          '#weight' => 2,
        );
      }
    }

    //### User Endpoint Settings
    if( $endpoint === Endpoints::MEMBER ){
      $element['appreciation_engine_api_settings'][$endpoint]['userRefreshInterval'] = array(
        '#type' => 'select',
        '#title' => t('Force-Refresh Cooldown'),
        '#description' => t('How frequently should users be able to force a refresh of their profile data, re-indexing their social activity.'),
        '#options' => $time_options,
        '#default_value' => isset($defaults['userRefreshInterval']) ? $defaults['userRefreshInterval'] : 500,
        '#weight' => 3,
      );
    }

    if(!empty($element)){
      // Add the endpoint settings to the form.
      $form['appreciation_engine_api_settings'][$endpoint] = array(
        '#type' => 'fieldset',
        '#title' => t($endpoint_name),
        '#weight' => $weight++,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => TRUE,
      );
      $form = array_merge_recursive($form, $element);
    }
  }

  $form['appreciation_engine_api_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Cache API Responses'),
    '#description' => t('<em>(Recommended)</em> Locally caching greatly improves performance and lowers API quota consumption. Only disable this for development purposes.'),
    '#default_value' => variable_get('appreciation_engine_api_cache', '1'),
    '#weight' => 3,
    '#tree' => TRUE,
  );

  $form = system_settings_form($form);

  $form['actions']['clear_cache'] = array(
    '#type' => 'submit',
    '#value' => t('Clear API Cache'),
    '#submit' => array('appreciation_engine_api_cache_clear_submit'),
  );

  return $form;
}

function appreciation_engine_api_cache_clear_submit(){
  appreciation_engine_api_cache_clear('*', TRUE);
}

/**
 * Implements hook_help()
 */
function appreciation_engine_api_common_help($path, $arg){
  switch($path){
    case 'admin/help#appreciation_engine_api':
      $filepath = dirname(__FILE__) . '/README.md';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }
      else {
        $filepath = dirname(__FILE__) . '/README.txt';
        if (file_exists($filepath)) {
          $readme = file_get_contents($path);
        }
      }
      if (!isset($readme)) {
        return NULL;
      }
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }else {
          $output = '<pre>' . $readme . '</pre>';
        }
      }else {
        $output = '<pre>' . $readme . '</pre>';
      }
      return $output;
  }
}

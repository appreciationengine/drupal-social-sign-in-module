<?php

/**
 * Implements hook_menu().
 */
function ae_social_login_menu() {
  $items = array();

  $items['ae_social_login'] = array(
    'title' => 'AE Social Login Page',
    'page callback' => '_ae_social_login_page',
    'access arguments' => array('administer appreciation_engine'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/people/appreciation_engine/widget_settings'] = array(
    'title' => 'Widget Settings',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_widget_settings_services_form'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 1,
  );
  $items['admin/config/people/appreciation_engine/widget_settings/services'] = array(
    'title' => 'Services',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_widget_settings_services_form'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 0,
  );
  $items['admin/config/people/appreciation_engine/widget_settings/user_messaging'] = array(
    'title' => 'User Messaging',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_widget_settings_user_messaging_form'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 1,
  );
  $items['admin/config/people/appreciation_engine/widget_settings/flow_and_behaviour'] = array(
    'title' => 'Flow / Behaviour',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_widget_settings_flow_and_behaviour_form'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 2,
  );
  $items['admin/config/people/appreciation_engine/widget_settings/email_format'] = array(
    'title' => 'Email Formatting',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_widget_settings_email_format_form'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 3,
  );
  //### Data / Business Rules
  $items['admin/config/people/appreciation_engine/data_business_rules'] = array(
    'title' => 'Data / Business Rules',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_data_business_rules_extra_fields_form'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 2,
  );
  //#### Extra Fields
  $items['admin/config/people/appreciation_engine/data_business_rules/extra_fields'] = array(
    'title' => 'Extra Fields',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_data_business_rules_extra_fields_form'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 0,
  );
  $items['admin/config/people/appreciation_engine/data_business_rules/extra_info'] = array(
    'title' => 'Extra Info',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_data_business_rules_extra_info_form'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 1,
  );
  $items['admin/config/people/appreciation_engine/data_business_rules/flow_text'] = array(
    'title' => 'Flow Text',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_data_business_rules_flow_text_form'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 2,
  );
  $items['admin/config/people/appreciation_engine/data_business_rules/business_rules'] = array(
    'title' => 'Business Rules',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_data_business_rules_business_rules_form'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 3,
  );
  $items['admin/config/people/appreciation_engine/data_business_rules/field_mapping'] = array(
    'title' => 'Field Mapping',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_data_business_rules_field_mapping_form'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 4,
  );

  //### Form Settings
  $items['admin/config/people/appreciation_engine/forms'] = array(
    'title' => 'Drupal Forms',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_forms_default_form_settings'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 3,
  );
  //#### Form Settings
  $items['admin/config/people/appreciation_engine/forms/settings'] = array(
    'title' => 'Form Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_social_login_forms_default_form_settings'),
    'access arguments' => array('administer appreciation_engine'),
    'weight' => 0,
  );

  //### AE User login AJAX endpoint
  $items['api/ae_social_login/user'] = array(
    'page callback' => '_ae_social_login_user_handler',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  //### User Login Forms
  $items['user/ae/required_fields'] = array(
    'title' => 'Additional Info',
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_required_fields'),
    'access callback' => 'ae_social_login_access_callback',
    'access arguments' => array(),
    'weight' => 0,
  );

  // Verify Email Form
  $items['user/ae_verify_email/%/%/%'] = array(
    'title' => 'Verify Email',
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_verify_email', 2, 3, 4 ),
    'access callback' => TRUE,
  );

  // Reset Password Form
  $items['user/ae_reset_password/%/%/%'] = array(
    'title' => 'Reset Password',
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ae_reset_password_ok', 2, 3, 4 ),
    'access callback' => TRUE,
  );

  return $items;
}

/**
 * Access callback for menu items
 *
 * @return bool
 */
function ae_social_login_access_callback(){
  if(isset($_POST['ae_user']) || isset($_SESSION['extra_fields_ae_user'])){
    return TRUE;
  }else{
    return FALSE;
  }
}

/**
 * Implements hook_entity_info().
 */
function ae_social_login_entity_info() {
  $entity_info = array(
    'label' => t('AE User'),
    'plural label' => t('AE Users'),
    'entity class' => 'Appreciation_Engine\AE_User',
    'controller class' => 'AE_UserEntityController',
    'base table' => 'ae_users',
    'module' => 'ae_social_login',
    'entity keys' => array(
      'id' => 'ae_uid',
    ),
    'fieldable' => TRUE,
  );

  if(module_exists('entitycache')){
    $entity_info['entity cache'] = TRUE;
  }

  return array(
    'AE_User' => $entity_info
  );
}

/**
 * Implements hook_theme()
 */
function ae_social_login_theme($existing, $type, $theme, $path){
  $module_path = drupal_get_path('module', 'ae_social_login');
  return array(
    'ae_register_link' => array(
      'function' => 'theme_ae_register_link',
      'path' => $module_path . '/ae_social_login.theme.inc',
      'type' => 'module',
      'render element' => 'element',
      'variables' => [
        'social' => NULL,
        'remote_only' => NULL,
        'attributes' => [],
        'arguments' => NULL,
        'label' => NULL,
        'action' => NULL,
        'preposition' => NULL,
        'type' => NULL,
      ]
    )
  );
}

/**
 * Implements hook_stream_wrappers().
 *
 * Download AE profile images using includes/AEStreamWrapper.inc
 */
function ae_social_login_stream_wrappers() {
  return array(
    'appreciation-engine' => array(
      'name' => t('Appreciation Engine Photos'),
      'class' => 'AEStreamWrapper',
      'description' => t('Profile Images from Appreciation Engine'),
      'type' => STREAM_WRAPPERS_READ_VISIBLE,
    ),
  );
}

/**
 * Implements hook_modules_enabled()
 */
function ae_social_login_modules_enabled($modules) {
  if (in_array('entitycache', $modules) && !db_table_exists('cache_entity_AE_User') ) {
    $cache_schema = drupal_get_schema_unprocessed('system', 'cache');
    $cache_schema['description'] = "Cache table used to store AE_User entity records.";
    db_create_table('cache_entity_AE_User', $cache_schema);
  }
}
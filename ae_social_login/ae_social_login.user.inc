<?php
module_load_include('inc', 'ae_social_login', 'ae_social_login.ae_user');
use Appreciation_Engine\AE_User;

/**
 * Implements hook_user_update().
 *
 */
function ae_social_login_user_update(&$edit, $account){
  if(isset($account->logintoboggan_email_validated)){
    $ae_user = AE_User::getData($account->uid);
    if(!isset($ae_user)){
      // If this user has been validated by logintoboggan
      list($ae_uid) = AE_User::get_pending_verification(NULL, $account);
      if(isset($ae_uid)){
        $ae_user = AE_User::getMemberById($ae_uid);
        if(isset($ae_user->data)){
          // Create a new $AE_User for this $account
          $AE_User = new AE_User($ae_user, $account);
          $AE_User->save();
        }
      }
    }
  }
  if(!module_exists('profile2')){
    AE_User::update($account, 'user');
  }
}

/**
 * Update the AE user after the profile2 has been saved.
 *
 * @param $profile
 */
function ae_social_login_profile2_update($profile) {
  $account = user_load($profile->uid);
  $account->{'profile_'.$profile->type} = $profile;
  AE_User::update($account, 'user');
}

/**
 * Delete AE User record when an account is deleted
 * @param $account
 */
function ae_social_login_user_delete($account){
  $AE_User = AE_User::load($account->uid);
  if(isset($AE_User)){
    $AE_User->delete();
  }
}

function ae_social_login_user_load($users){
  foreach( $users as $uid => &$account){
    if($uid == 0){
      $account->ae_user = NULL;
      continue;
    }
    $ae_user = AE_User::getData($uid);
    if(isset($ae_user)){
      $account->ae_user = $ae_user;
    }else{
      $account->ae_user = NULL;
    }
  }
}

/**
 * Copied from logintoboggan_form_user_pass_reset_alter
 * @param $form
 * @param $form_state
 */
function ae_social_login_form_user_pass_reset_alter(&$form, &$form_state) {
  if (arg(5) != 'login' && ($uid = (int) arg(2))) {
    if ($account = user_load($uid)) {
      // Verify the AE user's email because they've verified it via the Drupal password reset email.
      $ae_user = AE_User::getData($uid);
      if(isset($ae_user)){
        AE_User::verify_email($account->mail, $ae_user);
      }else{
        // If there was a pending verification
        list($ae_uid) = AE_User::get_pending_verification(NULL, $account);
        if(isset($ae_uid)){
          $ae_user = AE_User::getMemberById($ae_uid);
          if(isset($ae_user->data)){
            // Create a new $AE_User for this $account
            $AE_User = new AE_User($ae_user, $account);
            $AE_User->save();
          }
        }
      }
    }
  }
}

/**
 * When the user updates their Drupal account password update their AE password as well.
 * @param $form
 * @param $form_state
 */
function ae_social_login_user_profile_password_change_submit($form, &$form_state){
  if(!empty($form_state['values']['pass'])){
    // Ignore if a new user was created
    $user = $form_state['user'];
    $pass_reset = isset($_SESSION['pass_reset_' . $user->uid]) && isset($_GET['pass-reset-token']) && ($_GET['pass-reset-token'] == $_SESSION['pass_reset_' . $user->uid]);
    $ae_user = NULL;
    if($pass_reset && $user->mail == $form_state['values']['mail']){
      // if the user's password has been reset via email then we can trust this email & get the user.
      $result = AE_User::getMemberByEmail($user->mail, 'email');
      if(isset($result->data)){
        $ae_user = $result;
      }
    }
    if(!isset($ae_user)){
      // Otherwise, we only trust ae_users already tied to this account.
      $AE_User = AE_User::load($user->uid);
      if(isset($AE_User)){
        $ae_user = $AE_User->ae_data;
      }
    }
    $password = $form_state['values']['pass'];
    if(isset($ae_user)){
      $ae_user = AE_User::reset_password($ae_user, $password);
      $AE_User = NULL;
      if(isset($ae_user->data)){
        $AE_User = new AE_User($ae_user, $user);
      }
      if($AE_User == NULL){
        // If this account is verified, see if we can connect the AE User.
        $account = user_load($user->uid);
        if(AE_User::isVerified($ae_user, $account)){
          $AE_User = new AE_User($ae_user, $account);
          $AE_User->save();
        }
      }else{
        // After a password reset a new access token is generated, so we should save it for the cache.
        $AE_User->save();
      }
    }else if(variable_get('ae_social_login_user_profile_register', 0) == 1){
      $account = user_load($user->uid);
      $AE_User = AE_User::register_account($account, $password);
    }
  }
}

/**
 * user_profile_form form_alter
 * @param $form
 * @param $form_state
 */
function ae_social_login_form_user_profile_form_alter(&$form, &$form_state){
  array_unshift($form['#submit'], 'ae_social_login_user_profile_password_change_submit');
}

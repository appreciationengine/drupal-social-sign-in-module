/**
 * AE Social Login Drupal Behaviors
 */
/*globals jQuery, Drupal, IGA, aeJS */
(function($) {
	"use strict";
	var dsae = Drupal.settings.ae_social_login,
		dbae = Drupal.behaviors.ae_social_login = {
		$block: $("#block-ae-social-login-ae-social-login"),
		isReady: false,
		remoteOnly: false,
		auth: "register",
		isMobile: dsae && dsae.settings ? !dsae.settings.auth_window : false,
		ready: function(aeJS){
			if(dbae.$block.length){
				if(Drupal.settings.ae_social_login.override_login !== true){
					aeJS.events.onLogin.addHandler(dbae.login);
				}
				if(Drupal.settings.ae_social_login.override_logout !== true){
					aeJS.events.onLogout.addHandler(dbae.logout);
				}
			}
			// Make sure aeJS exists in the global scope.
			window.aeJS = window.aeJS || aeJS;

			if( dsae.smartAuth === true ){
				if(window.sessionStorage){
					// If the user has logged in earlier in this session, consider
					// it safe to merge their account.
					dbae.auth = sessionStorage.getItem("ae_social_login.auth") || dbae.auth;
				}
				if(dbae.auth === "auth"){
					dbae.updateAuth();
				}else{
					aeJS.events.onLogin.addHandler(dbae.updateAuth);
				}
				aeJS.events.onLogout.addHandler(function(){
					dbae.auth = "register";
					if(window.sessionStorage){
						sessionStorage.removeItem("ae_social_login.auth");
					}
				});
			}

			if(location.href.indexOf("accessToken=") > 0 && typeof history.pushState === "function"){
				// We don't want the user sharing a page with an accessToken.
				history.pushState(null, document.title, location.pathname);
			}

			aeJS.events.onMobileDetect.addHandler(function(){ dbae.isMobile = true; });
			dbae.isReady = true;
			dbae.attach(document, Drupal.settings.ae_social_login);

			for(var c in readyCallbacks){
				readyCallbacks[c](aeJS);
			}
			readyCallbacks = [];

			function element_login(ae_user, state, sso){
				if(state !== 'init'){
					var $el = dbae.getTriggeringElement();
					if($el){ $el.trigger("ae_login", [$el , ae_user, state, sso]); }
				}
			}
			aeJS.events.onLogin.addHandler(element_login);
		},
		onReady: function(callback){
			if(typeof callback !== "function"){ return; }
			if(dbae.isReady){
				callback(aeJS);
			}else{
				readyCallbacks.push(callback);
			}
		},
		attach: function(context, settings){
			if(!dbae.isReady){ return; }
			if($.cookie && $.cookie("ae_social_login_redirect_login")){
				$.cookie("ae_social_login_redirect_login", null, { path:'/' });
				dsae.redirect_login = true;
				var redirect_required_fields = $.cookie("ae_social_login_redirect_required_fields");
				if(redirect_required_fields){
					dsae.redirect_required_fields = redirect_required_fields;
					$.cookie("ae_social_login_redirect_required_fields", null, { path:'/' });
				}
			}
			var $context = $(context);
			if(context !== document){
				// Attach to any forms loaded outside of the document ex. via ajax
				$context.once("ae_social_login", function(){
					// setup all AE register / login forms
					if($context.is("form[data-ae-register-form]")){
						dbae.attachForm($context, settings);
					}else{
						// We want to call aeJS.trigger.attach on a form, not a container, otherwise all forms will get re-attached.
						$context.find("form[data-ae-register-form]").each(function(){
							dbae.attachForm($(this), settings, true);
						});
					}
				});
			}else{
				$context.find("form[data-ae-register-form]").each(function(){
					dbae.attachForm($(this), settings, false);
				});
				//! BUGFIX AEJS requires a re-attach of data-ae-register-link onLogin
				aeJS.events.onLogin.addHandler(function(){
					dbae.attachLinks(document);
				});
			}

			dbae.attachLinks(context);

			// Add an AE logout to all Drupal logout links
			$('a[href="/user/logout"]', context).bind('click', function(e) {
				if(aeJS){ aeJS.trigger.logout(); }
			});
		},
		attachForm: function($form, settings, attach){
			var form_id = $form.find("input[name=form_id]").val(),
				submitting = false;
			function submit(ae_user, type){
				//### After the AE login
				if(!submitting){ return; }
				if(type === "init"){ return; }
				var $ae_user = $form.find("input[name=ae_user]");
				// update the form with the ae user
				$ae_user.val(JSON.stringify(ae_user));
				$form[0].action = location.pathname + location.search;
				dbae.updateForm($form);
				// and submit.
				$form.unbind('submit');
				$form.submit();
				submitting = false;
			}

			function error(err){
				// Display AE login error messages
				if(!submitting){ return; }
				if(err.message){
					$(".messages").first().addClass("messages--error").html(err.message).show();
				}
			}

			if($.inArray(form_id.replace(/_/g,"-"), settings.forms) > -1){// settings.forms.indexOf()
				var $formSubmit = $form.find(".form-submit");
				if(attach){
					aeJS.trigger.attach($form[0]);
				}
				$formSubmit.on("mousedown", function(e){
					// When the user submits the form copy field values to their ae fields
					dbae.map_ae_fields($form);
				});
				// or opens any form-related AE links
				$form.find("a.ae-register-link").on("click", function(){
					submitting = true;
				});
				$form.on("submit", function(){ submitting = true; });

				if(aeJS.user.data){
					aeJS.events.onUser.addHandler(submit);
				}else{
					aeJS.events.onLogin.addHandler(submit);
				}
				aeJS.events.onFlow.addHandler(function(e){
					if(e.step === "error"){ error(e.error); }
				});
			}
		},
		setRemoteOnly: function(){
			function nextWindow(state){
				if(state.state === "closed"){
					dbae.remoteOnly = false;
					aeJS.events.onWindow.removeHandler(nextWindow);
					aeJS.events.onLogin.removeHandler(nextLogin);
					aeJS.settings.return_url = dsae.settings.return_url;
				}
			}
			function nextLogin(user, type){
				setTimeout(function(){
					dbae.remoteOnly = false;
					aeJS.events.onWindow.removeHandler(nextWindow);
					aeJS.events.onLogin.removeHandler(nextLogin);
					aeJS.settings.return_url = dsae.settings.return_url;
				}, 0);
			}
			dbae.remoteOnly = true;
			aeJS.events.onWindow.addHandler(nextWindow);
			aeJS.events.onLogin.addHandler(nextLogin);
			aeJS.settings.return_url = location.pathname + "?remoteOnly=1";
		},
		attachLinks: function(context){
			$("a.ae-register-link[data-remote-only]", context).once("ae_social_login", function(){
				// Handle remote registration only links
				var $link = $(this);
				$link.on("click", function(){
					dbae.setRemoteOnly();
				});
			});

			function onUser(user){
				for(var s in user.services){
					var service = user.services[s].Service;
					$("a.ae-register-link[data-ae-register-link="+service+"], a.ae-register-link[data-ae-login-link="+service+"], a.ae-register-link[data-ae-auth-link="+service+"]").addClass("connected");
				}
			}
			aeJS.events.onUser.addHandler(onUser);
			aeJS.events.onLogin.addHandler(onUser);
			aeJS.events.onLogout.addHandler(function(){
				$("a.ae-register-link").removeClass("connected");
			});

		},
		updateAuth: function(user, type, sso){
			if(sso){ return; }
			dbae.auth = "auth";
			if(window.sessionStorage){
				// Keep track of auth in sessionStorage
				sessionStorage.setItem("ae_social_login.auth", "auth");
			}
			$("a[data-ae-register-link]").each(function(){
				var $this = $(this), social = $this.attr("data-ae-register-link");
				$this.attr("data-ae-register-link", null).attr("data-ae-auth-link", social);
				aeJS.trigger.attach($this);
			});
			$("a[data-ae-login-link]").each(function(){
				var $this = $(this), social = $this.attr("data-ae-login-link");
				$this.attr("data-ae-login-link", null).attr("data-ae-auth-link", social);
				aeJS.trigger.attach($this);
			});
		},
		//## AE Block login callback
		login: function(user){
			if(dbae.remoteOnly){ return; }
			if(user && user.data.AccessToken && $("body").hasClass("not-logged-in")){
				$.ajax({
					url: window.location.origin+"/api/ae_social_login/user",
					type: "POST",
					async: true,
					data: {user: user},
					beforeSend: function(){
						$(".ae_bar_loader").hide();
						$(".ae_social_login_services").prepend("<div class='ae_bar_loader'></div>");
					},
					success: function(response) {
						var user = response.match(/\"uid\"/g) ? JSON.parse(response) : null;
						if(user && user.uid){ //check that valid user object is returned
							//TODO fire an event before reload
							window.location.reload();
						} else {
							//TODO fire an error
							$(".ae_bar_loader").replaceWith('<p>There was an error signing in</p>');
						}
					}
				});
			}
		},
		logout: function(){
			window.location.href= "/user/logout";
		},
		map_ae_fields: function($form){
			dbae.remove_ae_field_mappings($form);
			// Note bind = true only works for manual user input, to cover autocomplete, call map fields just before AE submission.
			if(!Drupal.settings.ae_social_login.form_field_mapping){ return; }
			var form_id = $form.find("input[name=form_id]").val();
			$.each(Drupal.settings.ae_social_login.form_field_mapping, function(drupal_field_path, ae_field){
				var dFieldPath = drupal_field_path.split('.');
				// If this mapping is for this form
				if(dFieldPath.length < 2 || dFieldPath[0] !== form_id ){ return; }
				var field_name = dFieldPath[1];
				// If the Drupal field exists on the form,
				var $drupal_field = $form.find('input[name="'+field_name+'"], .form-item-'+field_name).last(),
					$ae_field = $form.find('input[name="'+ae_field+'"]'), ae_val;
				//Note
				if($drupal_field.length > 0 && $drupal_field.is("input") && $ae_field.length === 0){
					// and the AE field doesn't exist,
					ae_val = $drupal_field.val();
					if(!!ae_val){
						// and the field has a value.
						$ae_field = $("<input class='ae-field-mapping-field' name='"+ae_field+"' type='hidden' value='"+ae_val+"' />");
						$form.append($ae_field);
					}
				}else if(ae_field === "birthdate" && $ae_field.length === 0){
					// Handle drupal date select fields
					var $day = $drupal_field.find("select.day, input.day, select[name$=\"[day]\"]"),
						$month = $drupal_field.find("select.month, input.month, select[name$=\"[month]\"]"),
						$year = $drupal_field.find("select.year, input.year, select[name$=\"[year]\"]"),
						day = $day.val(),
						month = $month.val(),
						year = $year.val(),
						ae_birthdate = null,
						today = new Date().toISOString().split("T")[0];
					if(month){ month = ("00" + month).slice(-2); }
					if(day){ day = ("00" + day).slice(-2); }
					if(year && month && day){ ae_birthdate = year + '-' + month + '-' + day; }
					if(ae_birthdate && ae_birthdate !== today){
						// Add the birthdate if it has been set / changed.
						$ae_field = $("<input class='ae-field-mapping-field' name='"+ae_field+"' type='hidden' value='"+ae_birthdate+"' />");
						$form.append($ae_field);
					}
				}
			});
			return this;
		},
		remove_ae_field_mappings: function($form){
			$form.find("input.ae-field-mapping-field").remove();
			return this;
		},
		updateForm: function($form){
			dbae.remove_ae_field_mappings($form);
			// Update form fields with values from the AE user
			var form_id = $form.find("input[name=form_id]").val();
			if( !aeJS.user || !aeJS.user.data ){ return; }
			$.each(Drupal.settings.ae_social_login.form_field_mapping, function(drupal_field_path, ae_field){
				var dFieldPath = drupal_field_path.split('.');
				// If this mapping is for this form
				if(dFieldPath.length < 2 || dFieldPath[0] !== form_id ){ return; }
				var field_name = dFieldPath[1];
				for(var dataField in aeJS.user.data){
					if(dataField.toLowerCase() === ae_field){
						var $drupal_field = $form.find('input[name="'+field_name+'"], .form-item-'+field_name).last();
						if($drupal_field.length > 0 && $drupal_field.is("input")){
							$drupal_field.val(aeJS.user.data[dataField]);
						}else if(ae_field === "birthdate"){
							var $day = $drupal_field.find("select.day, input.day, select[name$=\"[day]\"]"),
								$month = $drupal_field.find("select.month, input.month, select[name$=\"[month]\"]"),
								$year = $drupal_field.find("select.year, input.year, select[name$=\"[year]\"]"),
								dob = aeJS.user.data.BirthDate.split('-');
							if(dob.length === 3){
								$day.val(parseInt(dob[2]));
								$month.val(parseInt(dob[1]));
								$year.val(dob[0]).trigger("change");
							}
						}
					}
				}
			});
		},
		$trigggeringEl: null,
		getTriggeringElement: function(){
			try{
				if(dbae.$trigggeringEl){ return dbae.$trigggeringEl; }
				var elId = $.cookie("ae_login_triggering_element");
				if(elId){
					return $(document.getElementById(elId));
				}
			}finally{
				dbae.clearTriggeringElement();
			}
		},
		setTriggeringElement: function($el){
			dbae.$trigggeringEl = $el;
			if($el && $el[0].id && dbae.isMobile){
				$.cookie("ae_login_triggering_element", $el[0].id, { path:'/' });
			}
		},
		clearTriggeringElement: function(){
			dbae.$trigggeringEl = null;
			$.cookie("ae_login_triggering_element", null, { path:'/' });
		}
	},
	readyCallbacks = [];
})(jQuery);

<?php

/**
 *  @file Create a Stream Wrapper class for AE user profile images.
 */
class AEStreamWrapper extends DrupalLocalStreamWrapper {

  // Overrides $base_url defined in MediaReadOnlyStreamWrapper.
  protected $base_url = 'http://beta.theappreciationengine.com';

  protected $path_args = array();

  //TODO periodically  / if changed refresh avatar image
  //@see https://api.drupal.org/api/drupal/modules!image!image.module/function/image_path_flush/7

  function getExternalUrl() {
    $path_args = $this->getPathArgs();
    if(empty($path_args)){ return ''; }
    $use_local_avatar = variable_get('ae_use_local_avatar', 1) == 1;

    return $this->getRemoteUrl(NULL);
    //!BUG local user images have been causing some weird issues with DrupalLocalStreamWrapper::url_stat & DrupalLocalStreamWrapper::getLocalPath taking excessively long to complete.
    if(isset($path_args['styles']) || $use_local_avatar){
      // image style
      $local_path = $this->getPhysicalLocalPath();
      if(!is_file($this->uri) && isset($path_args['styles'])){
        //### Check if this image style version has been created
        $style = image_style_load($path_args['styles']);
        $original_local_path = $this->getOriginalLocalPath();
        image_style_create_derivative($style, $original_local_path, $local_path);
      }
      return file_create_url( $local_path );
    }else{
      // return source image
      return $this->getRemoteUrl(NULL);
    }
  }

  function getRemoteUrl($scheme = 'http'){
    $ae_domain = variable_get('ae_domain', $this->base_url);
    $parts = explode('://', $ae_domain);
    if(count($parts) > 1){
      $ae_domain = (!empty($scheme)?$scheme.':':'').'//'.$parts[1];
    }
    $path_args = $this->getPathArgs();
    $memberID = $this->getID($path_args['avatar']);
    return $ae_domain. '/Member_Controller/getAvatar/' . $memberID;
  }
  
  function getID($avatar){
    return $memberID = check_plain(str_replace('.jpg', '', $avatar));
  }

  function getOriginalLocalPath(){
    $path_args = $this->getPathArgs();
    $local_path = file_default_scheme() . '://' . $this->getDirectoryPath();
    if(isset($path_args['avatar'])){
      $memberID = $this->getID($path_args['avatar']);
      $local_path .= 'avatar/' . $memberID . '.jpg';
    }
    return $local_path;
  }

  function getPhysicalLocalPath(){
    return str_replace('appreciation-engine://', file_default_scheme() . "://" . $this->getDirectoryPath() , $this->uri);
  }

  function getLocalPath($uri = NULL) {
    //uri argument not used but needed for compatibility with parent
    //### Check if the original image has been downloaded
    $original_local_path = $this->getOriginalLocalPath();
    if (!file_exists($original_local_path)) {
      $dirname = drupal_dirname($original_local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      $response = drupal_http_request($this->getRemoteUrl(NULL), array(
        'timeout' => 1.0, // Don't wait more than 1 second for the image response.
      ));
      if (!isset($response->error)) {
        file_unmanaged_save_data($response->data, $original_local_path, TRUE);
      }
    }

    $local_path = $this->getPhysicalLocalPath();
    return $local_path;
  }

  /**
   * Implements abstract public function getDirectoryPath()
   */
  public function getDirectoryPath() {
    return 'appreciation-engine/';
  }

  /**
   * @see MediaReadOnlyStreamWrapper->_parse_url
   * @param $url
   * @return array|bool
   */
  protected function parse_url($url) {
    $path = explode('://', $url);
    $parts = explode('/',  $path[1]);
    $params = array();
    $count = 0;
    $total = count($parts);
    while ($count < $total) {
      if($parts[$count] == 'appreciation-engine' ){
        // allow this part of the styles urls
        $params[$parts[$count++]] = 'appreciation-engine';
        continue;
      }
      if(!isset($parts[$count+1])){ break; }
      // We iterate count for each step of the assignment to keep us honest.
      $params[$parts[$count++]] = $parts[$count++];
    }
    return $params;
  }

  protected function getPathArgs(){
    if( empty($this->path_args) ){
      $this->path_args = $this->parse_url($this->uri);
    }
    return $this->path_args;
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'image/jpeg';
  }

  /**
   * Read Only
   * @param null $f
   * @return bool|string
   */
  function getTarget($f = NULL) {
    return FALSE;
  }

  /**
   * @see remote_stream_wrapper
   */
  private function remote_stream_stat() {
    $stat = array();
    $request = drupal_http_request($this->getRemoteUrl(), array('method' => 'HEAD'));
    if (empty($request->error)) {
      if (isset($request->headers['content-length'])) {
        $stat['size'] = $request->headers['content-length'];
      }else if ($size = strlen($request->data)) {
        // If the HEAD request does not return a Content-Length header, fall
        // back to performing a full request of the file to determine its file
        // size.
        $stat['size'] = $size;
      }
    }

    return $this->getStat($stat);
  }

  public function url_stat($uri, $flags) {
    //$use_local_avatar = variable_get('ae_use_local_avatar', 1) == 1;
    //if($use_local_avatar){
    //  return parent::url_stat($uri, $flags);
    //}

    $this->uri = $uri;
    if ($flags & STREAM_URL_STAT_QUIET) {
      return @$this->remote_stream_stat();
    }
    else {
      return $this->remote_stream_stat();
    }
  }

  /**
   * Helper function to return a full array for stat functions.
   */
  protected function getStat(array $stat = array()) {
    $defaults = array(
      'dev' => 0,      // device number
      'ino' => 0,      // inode number
      'mode' => 0100000 | 0444,     // inode protectio
      'nlink' => 0,    // number of links
      'uid' => 0,      // userid of owner
      'gid' => 0,      // groupid of owner
      'rdev' => -1,    // device type, if inode device *
      'size' => 0,     // size in bytes
      'atime' => 0,    // time of last access (Unix timestamp)
      'mtime' => 0,    // time of last modification (Unix timestamp)
      'ctime' => 0,    // time of last inode change (Unix timestamp)
      'blksize' => -1, // blocksize of filesystem IO
      'blocks' => -1,  // number of blocks allocated
    );

    $return = array();
    foreach (array_keys($defaults) as $index => $key) {
      if (!isset($stat[$key])) {
        $return[$index] = $defaults[$key];
        $return[$key] = $defaults[$key];
      }
      else {
        $return[$index] = $stat[$key];
        $return[$key] = $stat[$key];
      }
    }

    return $return;
  }

}

<?php

/**
 * Implements hook_views_data().
 */
function ae_social_login_views_data() {

  // ## Define ae_users table
  $data['ae_users']['table']['group'] = t('AE Users');

  $data['ae_users']['table']['base'] = array(
    'field' => 'ae_uid',
    'title' => t('AE Users'),
    'weight' => 0,
    'defaults' => array(
      'field' => 'ae_uid',
    ),
  );

  $data['ae_users']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    )
  );

  // ## Define fields on the ae_users table
// ### uid
  $data['asf_spotify_presave_entrants']['uid'] = array(
    'title' => t('Uid'),
    'help' => t('The user ID'),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'name',
    ),
    'filter' => array(
      'title' => t('Name'),
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'title' => t('User'),
      'help' => t('Relate ae_users to an Drupal user account.'),
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'uid',
      'field' => 'uid',
      'label' => t('user'),
    ),
  );

  // ### ae_data
  $data['ae_users']['ae_data'] = array(
    'title' => t('AE User Data'),
    'help' => t('The Appreciation Engine User Data.'),
    'field' => array(
      'handler' => 'views_handler_field_ae_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'ae_uid',
      'numeric' => TRUE,
      'validate type' => 'numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

}
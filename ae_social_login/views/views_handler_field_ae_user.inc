<?php
/**
 * @file
 * Defines a field handler for an ae_user serialized field.
 */
use Appreciation_Engine\AE_User;

/**
 * Field handler to show data from a serialized ae_user column.
 *
 * @see views_handler_field_serialized
 * @ingroup views_field_handlers
 */
class views_handler_field_ae_user extends views_handler_field {

  function option_definition() {
    $options = parent::option_definition();
    $options['dataField'] = array('default' => 'id');
    return $options;
  }

  /**
   * Provide an option to select which firled from the AE user to display.
   */
  function options_form(&$form, &$form_state) {
    $options = [ 'id' => 'ID' ] + AE_User::$DataFields;
    $form['dataField'] = array(
      '#type' => 'select',
      '#title' => t('Data Field'),
      '#description' => t('Select the field from the AE User to display.'),
      '#options' => $options,
      '#default_value' => $this->options['dataField'],
    );

    parent::options_form($form, $form_state);
  }

  function render($values) {
    $value = $values->{$this->field_alias};
    if(!empty($value)){
      $ae_user = unserialize($value);
      $dataField = $this->options['dataField'];
      $dataField = ($dataField == 'id') ? 'ID' : AE_User::$DataFields[$dataField];
      if(isset($ae_user->data) && isset($ae_user->data->{$dataField})){
        $dataValue = $ae_user->data->{$dataField};
        return check_plain($dataValue);
      }
    }
    return NULL;
  }
}

<?php

use Appreciation_Engine\AE_User;

class AE_UserEntityController extends EntityAPIController {

  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  //loads an ae user based on ae_uid (default), or email or uid
  public function load($ids = array(), $conditions = array()) {
    $ae_users = parent::load($ids, $conditions);
    foreach($ae_users as &$ae_user){
      $ae_user->is_new = FALSE;
    }
    reset($ae_users);
    return $ae_users;
  }

  public function delete($ids) {
    parent::delete($ids);
  }

  /**
   * Implements EntityAPIController->save
   * Saves an AE_user entity in the table ae_user
   *
   * @param stdClass $AE_User
   * @return bool|int
   */
  public function save($AE_User, $account = NULL, $update = TRUE) {
    if(!isset($account) && isset($AE_User->uid) ){
      $account = user_load($AE_User->uid);
    }
    if(!isset($account) ||!isset($account->uid) || $account->uid == 0){
      // Bail if the user doesn't exist yet.
      // user_save must be called to create a user account and profiles.
      return FALSE;
    }
    $ae_user = $AE_User->ae_data;

    if(!isset($AE_User->is_new)){
      // b/c the entity key is a remote id, we must check if this is a new user before calling parent::save
      $old_users = $this->load(array($AE_User->ae_uid));
      if(empty($old_users)){
        $AE_User->is_new = TRUE;
      }else{
        $AE_User->original = current($old_users);
      }
    }
    //if existing object not found, it creates a new one
    $return = parent::save($AE_User);
    if($update){
      $this->update_extras($ae_user, $account);
    }
    return $return;
  }

  /**
   * Update any extra non-ae_user fields and fields from other modules
   * @param $ae_user
   * @param $account
   */
  private function update_extras($ae_user, $account){
    $ACCOUNT_UPDATED = FALSE;
    if(empty($account->mail) && $email = AE_User::get_email($ae_user)){
      $account->mail = $email;
      $ACCOUNT_UPDATED = TRUE;
    }
    if(empty($account->picture) && module_exists("file_entity")){
      $avatar_file = file_uri_to_object( 'appreciation-engine://avatar/'. $ae_user->data->ID . '.jpg', TRUE );
      $account->picture = file_save($avatar_file);
      $ACCOUNT_UPDATED = TRUE;
    }
    if(module_exists("logintoboggan")){
      $validating_id = logintoboggan_validating_id();
      if( isset($account->roles[$validating_id]) && in_array($account->mail, AE_User::get_verified_emails($ae_user)) ){
        //AE user is verified & the verified email matches the Drupal $account email, so verify the account
        _logintoboggan_process_validation($account);
      }
    }

    // Map AE user fields to the local account
    $updated_fields = AE_User::map_fields($ae_user, $account);

    // Save account once here.
    if($ACCOUNT_UPDATED){
      //TODO we don't really need to trigger our own ae_social_login_user_update here...
      //TODO don't save the $account.
      user_save($account);
    }else{
      // Update user fields
      if($updated_fields['user'] == TRUE){
        field_attach_update('user', $account);
        entity_get_controller('user')->resetCache(array($account->uid));
      }
      // Update profile fields
      foreach($updated_fields['profile2'] as $profile_type => $profile_updated){
        if($profile_updated){
          $profile = profile2_load_by_user($account, $profile_type);
          field_attach_update('profile2', $profile);
          entity_get_controller('profile2')->resetCache(array($profile->pid));
        }
      }
    }
  }

}

<?php


/**
 * Implements hook_preprocess_page()
 * Loads in necessary js
 */
function ae_social_login_preprocess_page(){
  global $user;

  if(path_is_admin(current_path()) && user_access('administer site configuration')){
    // We don't need to load AE login on admin pages.
    return;
  }

  $framework_url = variable_get('ae_js_framework_url');
  if ($framework_url){
    // Load AE Framework JS for end users
    drupal_add_js($framework_url, array('type' => 'external', 'scope' => 'footer', 'defer' => TRUE, 'async' => TRUE, 'group' => JS_THEME, 'weight' => 20));

    $settings = [];
    // Get custom AEJS settings and pass through to javascript
    $ae_settings = variable_get('ae_settings_master', array());

    if(empty($ae_settings['services'])){
      $ae_settings['services'] = NULL;
    }

    if(isset($ae_settings['extra_fields_screen']) && $ae_settings['extra_fields_screen'] == 'disabled'){
      $ae_settings['extra_fields'] = array();
    }
    $settings['settings'] = $ae_settings;

    $ae_field_mapping = variable_get('ae_field_mapping', null);
    if($ae_field_mapping && !empty($ae_field_mapping)){
      $settings['field_mapping'] = $ae_field_mapping;
    }

    $smartAuth = variable_get('ae_social_login_smartAuth', TRUE);
    $settings['smartAuth'] = $smartAuth;
    $debug_mode = variable_get('ae_social_login_debug_mode', FALSE);
    $settings['debug'] = $debug_mode;

    $ae_social_login_forms = variable_get('ae_social_login_forms', array());
    $ae_forms = array();
    foreach($ae_social_login_forms as $form_id => $attach){
      if($attach == TRUE){
        $ae_forms[] = str_replace('_', '-', $form_id);
        if($form_id == 'user_login'){
          $ae_forms[] = 'user-login-block';
        }
      }
    }
    $settings['forms'] = $ae_forms;

    drupal_add_js([ 'ae_social_login' => $settings ], 'setting');
    drupal_add_library('ae_social_login', 'ae_social_login');
  }
  ae_social_login_add_aejsready_script(); //load custom widget js using $ae_settings assigned above
}

/**
 * Build script for the AEJSReady callback for once the AE framework script
 * has loaded.
 *
 * @see https://developer.appreciationengine.com/documentation/index
 */
function ae_social_login_add_aejsready_script(){
  $ae_ready = "
  function AEJSReady(aeJS) {
    aeJS.settings = Drupal.settings.ae_social_login.settings;
    Drupal.behaviors.ae_social_login.ready(aeJS);\n";

  $debug_mode = variable_get('ae_social_login_debug_mode', false);
  if($debug_mode){
    $ae_ready .= "console.log(aeJS.settings);
    aeJS.events.onFlow.addHandler(function(step){ console.log('AEJS onFlow:'); console.log(step); });
    aeJS.events.onUser.addHandler(function(user, state){ console.log('AEJS onUser ['+state+']:'); console.log(user); });
    aeJS.events.onLogin.addHandler(function(user, type){ console.log('AEJS onLogin ['+type+']:'); console.log(user); });\n";
  }

  drupal_alter('ae_jsready', $ae_ready );
  $ae_ready .= "\n  }\n";
  drupal_add_js($ae_ready, array( 'type' => 'inline', 'scope'=>'footer', 'group'=>JS_THEME, 'every_page' => TRUE ) );
}

/**
 * Page that can only be viewed by AE Social Login members
 *
 * @return mixed
 */
function _ae_social_login_page() {

  $page_array['ae_social_login_arguments'] = array(
    '#title' => t('Members Page'),
    '#items' => $items,
    //Theme hook with suggestion.
    '#theme' => 'item_list__ae_social_login',
  );
  return $page_array;
}



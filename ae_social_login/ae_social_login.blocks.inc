<?php
module_load_include('inc', 'ae_social_login', 'ae_social_login.ae_user');
use Appreciation_Engine\AE_User;

/**
 * Implements hook_block_info().
 */
function ae_social_login_block_info() {
  $blocks['ae_social_login'] = array(
    'info' => t('Appreciation Engine Social Login'),
    'cache' => DRUPAL_CACHE_PER_USER,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * Prepares the contents of the block.
 */
function ae_social_login_block_view($delta = '') {
  global $user;
  $ae_user = AE_User::getData($user->uid);
  $block = array();
  //Array to contain items for the block to render.
  $content = variable_get('appreciation_engine_app_info', array());

  $items = array();
  if($content && !isset($content->error)){
    $ae_available_services = variable_get('ae_available_services', array());
    // Iterate over the content set and format as links, excluding any unavailable services
    foreach ($content->Urls as $service) {
      $service_name = strtolower($service->Name);
      if( isset($ae_available_services[$service_name]) && $ae_available_services[$service_name] === $service_name){
        $items[] = array(
          'data' => "<a href='#' class='ae-social-login-service ae-register-link' data-ae-register-link='".strtolower($service->Name)."'><div title='".$service->Name."' class='".strtolower($service->Name)."'></div></a>",
        );
      }
    }
  }
  if (empty($items)) {
    $block['content']['error'] = array(
      '#theme' => 'html_tag__ae_social_login__block',
      '#tag' => 'h2',
      '#attributes' => array(
        'class' => 'ae_error',
      ),
      '#value' => t('You do not appear to have registered any social services with the Appreciation Engine'),
    );
  } else {
    if(user_access('access content') && $user->uid){
      $name = (isset($ae_user->data) && isset($ae_user->data->FirstName) && $ae_user->data->FirstName) ? $ae_user->data->FirstName : "there";
      $logout_link = ($name != "there") ? (str_replace("{{Firstname}}", $ae_user->data->FirstName, variable_get('ae_logout_link'))) : (str_replace("{{Firstname}}", "you", variable_get('ae_logout_link')));
      $avatar_url = (isset($ae_user->data) && isset($ae_user->data->AvatarURL)) ? $ae_user->data->AvatarURL : null;
      $block['content']['heading'] = array(
        '#theme' => 'html_tag__ae_social_login__block',
        '#tag' => 'h2',
        '#attributes' => array(
          'class' => 'ae_heading',
        ),
        '#value' => t("Hi {$name}!")."<sub><a href='user/logout'>".$logout_link."</a></sub>",
      );
      if($avatar_url){
        $block['content']['avatar'] = array(
          '#theme' => 'html_tag__ae_social_login__block',
          '#tag' => 'div',
          '#attributes' => array(
            'class' => 'ae_avatar',
          ),
          '#value' => "<img src='".$avatar_url."' />",
        );
      }
    } else {
      if(variable_get('ae_sign_in_call_to_action')){
        $block['content']['heading'] = array(
          '#theme' => 'html_tag__ae_social_login__block',
          '#tag' => 'h2',
          '#attributes' => array(
            'class' => 'ae_heading',
          ),
          '#value' => t(variable_get('ae_sign_in_call_to_action')),
        );
      }
      if(variable_get('ae_include_social')){
        $block['content']['logins'] = array(
          '#theme' => 'item_list__ae_social_login__block',
          '#items' => $items,
          '#attributes' => array(
            'class' => 'ae_social_login_services',
          ),
        );
      } else {
        $block['content']['register'] = array(
          '#theme' => 'html_tag__ae_social_login__block',
          '#tag' => 'a',
          '#attributes' => array(
            'href' => '#',
            'data-ae-register-window' => ' ',
            'class' => 'ae_register_button',
            'value' => 'Register',
          ),
          '#value' => 'Register',
        );
      }
    }

    $module_path = drupal_get_path('module', 'ae_social_login');
    $block['content']['#attached'] = array(
      'css' => array(
        $module_path.'/css/ae_social_login.css' => array('type' => 'file', 'scope' => 'header')
      ),
    );

    return $block;
  }
}

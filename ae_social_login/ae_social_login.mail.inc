<?php

class AE_Email {
  static $Default = array(
    'reset_password_subject' => "Replacement login information for [ae_user:username] at [site:name]",
    'reset_password_body' => "[ae_user:username],

A request to reset the password for your account has been made at [site:name].

You may now reset your password by clicking this link or copying and pasting it to your browser:

[ae_user:reset-password-url]

This link is valid for one day and nothing will happen if it's not used; it will lead you to a page where you can set your password and then login.

--  [site:name] team",
    'verify_email_subject' => "Email Verification for [ae_user:username] at [site:name]",
    'verify_email_body' => "[ae_user:username],

A request to verify your account has been made at [site:name].

You may now verify your account by clicking this link or copying and pasting it to your browser:

[ae_user:verify-email-url]

This link will verify your access to this account and log you in; this link is valid for one day and nothing will happen if it's not used.

--  [site:name] team",
  );
}

/**
 * Implements hook_mail().
 */
function ae_social_login_mail($key, &$message, $params) {
  $key_subject = $key . '_subject';
  $key_body = $key . '_body';
  $language = $message['language'];
  $langcode = isset($language) ? $language->language : NULL;
  $variables = array();
  if(isset($params['account'])){
    $variables['user'] = $params['account'];
  }
  if(isset($params['ae_user'])){
    $variables['ae_user'] = $params['ae_user'];
  }

  $mail_subject = variable_get('ae_mail_' . $key_subject, AE_Email::$Default[$key_subject]);
  $mail_subject = t($mail_subject, array(), array('langcode' => $langcode));
  $mail_subject = token_replace($mail_subject, $variables, array('language' => $language, 'callback' => 'ae_social_login_mail_tokens', 'sanitize' => FALSE, 'clear' => TRUE));

  $mail_body = variable_get('ae_mail_' . $key_body, AE_Email::$Default[$key_body]);
  $mail_body = t($mail_body, array(), array('langcode' => $langcode));
  $mail_body = token_replace($mail_body, $variables, array('language' => $language, 'callback' => 'ae_social_login_mail_tokens', 'sanitize' => FALSE, 'clear' => TRUE));

  $message['subject'] .= $mail_subject;
  $message['body'][] = $mail_body;
}

/**
 * Define token replacements for ae_social_login emails
 * @see user_mail_tokens
 *
 * @param $replacements
 * @param $data
 * @param $options
 */
function ae_social_login_mail_tokens(&$replacements, $data, $options) {
  if (isset($data['ae_user'])) {
    $ae_user = $data['ae_user'];
    $replacements['[ae_user:username]'] = isset($ae_user->data->Username) ? $ae_user->data->Username : '';
    $replacements['[ae_user:email]'] = isset($ae_user->data->Email) ? $ae_user->data->Email : '';
    $replacements['[ae_user:verified_email]'] = isset($ae_user->data->VerfiedEmail) ? $ae_user->data->VerfiedEmail : '';
    $replacements['[ae_user:firstname]'] = isset($ae_user->data->FirstName) ? $ae_user->data->FirstName : '';
    $replacements['[ae_user:surname]'] = isset($ae_user->data->Surname) ? $ae_user->data->Surname : '';
    $timestamp = REQUEST_TIME;
    $replacements['[ae_user:reset-password-url]'] = ae_social_login_verification_url( 'reset_password', $timestamp, $data['ae_user']);
    $replacements['[ae_user:verify-email-url]'] = ae_social_login_verification_url( 'verify_email', $timestamp, $data['ae_user']);
  }
}

function ae_social_login_verification_url($op = 'reset_password', $timestamp, $ae_user){
  $hash =  ae_social_login_rehash($timestamp, $ae_user);
  if(!empty($hash)){
    return url("user/ae_$op/{$ae_user->data->ID}/$timestamp/$hash", array('absolute' => TRUE));
  }else{
    watchdog('ae_social_login', "Unable to generate $op link");
    return '';
  }
}

/**
 * Generate a tokenized link based on an AE User lastUpdate time and
 * email account token.
 *
 * @param        $timestamp
 * @param        $ae_user
 * @param string $service
 * @return string
 */
function ae_social_login_rehash($timestamp, $ae_user, $service = 'email'){
  //TODO account
  foreach($ae_user->services as $ae_service){
    if($ae_service->Service == $service){
      break;
    }
  }
  // The token will be updated after an api call to reset_password
  $token = $ae_service->Token;
  return drupal_hmac_base64($timestamp . $ae_service->LastUpdated, drupal_get_hash_salt() . $token); //TODO . apiKey
}
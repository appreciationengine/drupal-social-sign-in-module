<?php
namespace Appreciation_Engine;
use Entity;
use EntityFieldQuery;
use Appreciation_Engine\API as AE_API;

/**
 * Class AE_User
 * @file AE User controller / interface methods
 * @package Appreciation_Engine
 */

class AE_User extends Entity {

  private static $_api = NULL;
  private static function api(){
    if(!isset(AE_User::$_api)){
      module_load_include('inc', 'appreciation_engine_api', 'appreciation_engine_api.class');
      AE_User::$_api = new AE_API();
    }
    return AE_User::$_api;
  }

  static $DataFields = array(
    'firstname' => 'FirstName',
    'surname'=> 'Surname',
    'email' => 'Email',
    'verfiedemail' => 'VerifiedEmail',
    'username' => 'Username',
    'website' => 'Website',
    'gender' => 'gender',
    'birthdate' => 'BirthDate',
    'city' => 'City',
    'state' => 'State',
    'country' => 'Country',
    'countrycode' => 'CountryCode',
    'postcode' => 'PostCode',
    'avatarurl' => 'AvatarUrl',
    'mobilephone' => 'MobilePhone',
    'homephone' => 'HomePhone',
    'address' => 'Address',
    'addressline2' => 'AddressLine2',
    'bio' => 'Bio',
    'geocountry' => 'GeoCountry',
    'geolat' => 'GeoLat',
    'geolong' => 'GeoLong'
  );

  public function __construct() {
    $args = func_get_args();
    $arg1 = isset($args[0]) ? $args[0] : array();
    $values = array();
    if(is_array($arg1)){
      $values = $arg1;
    }else if(is_object($arg1)){
      $arg2 = isset($args[1]) ? $args[1] : NULL;
      $values = AE_User::_constructAE_User($arg1, $arg2);
    }
    parent::__construct($values, 'AE_User');
  }

  private static function _constructAE_User($ae_user, $account){
    $values = array(
      'ae_uid' => isset($ae_user) ? $ae_user->data->ID : NULL,
      'uid' => isset($account) && isset($account->uid) ? $account->uid : NULL,
      'firstname' => isset($ae_user->data->FirstName) ? $ae_user->data->FirstName : NULL,
      'surname' => isset($ae_user->data->Surname ) ? $ae_user->data->Surname : NULL,
      'avatar_url' => isset($ae_user->data->AvatarURL) ? $ae_user->data->AvatarURL : NULL,
      'ae_data' => $ae_user,
      'verified_email' => isset($ae_user->data->VerifiedEmail) ? $ae_user->data->VerifiedEmail : NULL,
    );
    return $values;
  }

  /**
   * Retrieve an AE user based on a Drupal user id.
   *
   * @param $uid
   * @param bool $refresh
   * @return mixed
   */
  static function load($uid, $refresh = FALSE){
    $uid = intval($uid);
    $AE_Users = &drupal_static(__FUNCTION__, array());
    // If an AE_User has been added this request calling with $refresh == TRUE
    // will force a new EFQ.
    if(!isset($AE_Users[$uid]) || $refresh == TRUE){
      // Find the ae_users for this uid.
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'AE_User')
        ->propertyCondition('uid', $uid)
        ->propertyOrderBy('ae_uid', 'DESC');
      $AE_Users[$uid] = $query->execute();
    }

    if(!empty($AE_Users[$uid]['AE_User'])){
      // found an account matching this unverified
      $ae_uid = current($AE_Users[$uid]['AE_User'])->ae_uid;
      $ae_users = entity_load('AE_User', array($ae_uid));
      if(!empty($ae_users)){
        $AE_User = current($ae_users);
        return $AE_User;
      }
    }
    return NULL;
  }

  static function loadAEUser($ae_user){
    $ae_uid = $ae_user->data->ID;
    $ae_users = entity_load('AE_User', array($ae_uid));
    if(!empty($ae_users)){
      $AE_User = current($ae_users);
      return $AE_User;
    }
    return NULL;
  }


  /**
   * Retrieve an AE user data based on a Drupal user id.
   *
   * @param $uid
   * @param bool $refresh
   * @return ae_user | null
   */
  static function getData($uid, $refresh = FALSE){
    $AE_User = AE_User::load($uid, $refresh);
    if(isset($AE_User)){
      return $AE_User->ae_data;
    }
    return NULL;
  }

  /**
   * Get the Drupal userID for a local ae_user. Handles changes in ae_uid based on AE account merges.
   *
   * @param $ae_user
   * @return null
   */
  static function getUserID($ae_user){
    $uid = &drupal_static( __FUNCTION__);
    if(isset($uid)){ return $uid; }

    // Look for an existing {ae_user, uid} pair.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'AE_User')
      ->propertyCondition('ae_uid', $ae_user->data->ID);
    $result = $query->execute();

    $ae_uid = NULL;
    if(!empty($result['AE_User'])){
      // We've found an account matching this user.
      $ae_uid = current($result['AE_User'])->ae_uid;
      $ae_users = entity_load('AE_User', array($ae_uid));
      if(!empty($ae_users)){
        $_ae_user = current($ae_users);
        $uid = $_ae_user->uid;
      }
    }

    $verified_emails = AE_User::get_verified_emails($ae_user);
    if(!isset($uid) && !empty($verified_emails)){
      // ### Check against verified email in case ae_uid has changed due to a merge
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'AE_User')
        ->propertyCondition('verified_email', $verified_emails, 'IN');
      $result = $query->execute();

      if(!empty($result['AE_User'])){
        // found an account matching this unverified
        $ae_uid = current($result['AE_User'])->ae_uid;
        $ae_users = entity_load('AE_User', array($ae_uid));
        if(!empty($ae_users)){
          $_ae_user = current($ae_users);
          $uid = $_ae_user->uid;
          $AE_User = AE_User::load($uid);
          $AE_User->ae_data = $ae_user;
          entity_save('AE_User', $AE_User);
        }
      }
    }
    return $uid;
  }

  /**
   * Check if there is a local user for this $ae_user.
   *
   * @param $ae_user
   * @return bool
   */
  static function exists($ae_user){
    return AE_User::getUserID($ae_user) !== NULL;
  }

  /**
   * Get a remote AE user from the API
   *
   * @param $ae_uid
   * @param bool $CACHE
   * @return mixed
   */
  static function getMemberById($ae_uid, $CACHE = TRUE){
    $ae_uid = check_plain($ae_uid);
    return AE_User::api()->member(array( 'id' => $ae_uid), $CACHE);
  }

  /**
   * Get a remote AE user from the API based on the user's email and optional service.
   *
   * @param $email
   * @param null $service
   * @param bool $CACHE
   * @return mixed
   */
  static function getMemberByEmail($email, $service = NULL, $CACHE = TRUE){
    if(isset($service)){
      return AE_User::api()->member(array( 'username' => $email, 'service' => $service ), $CACHE);
    }else{
      return AE_User::api()->member(array( 'email' => $email ), $CACHE);
    }
  }

  /**
   * Check that an ae_user data object contains a valid user.
   * @param $ae_user
   * @return bool
   */
  static function validate(&$ae_user){
    $valid = FALSE;
    if( !empty($ae_user->data->AccessToken) ){
      $access_token = check_url(trim($ae_user->data->AccessToken));
      $result = AE_User::api()->auth( array( 'accessToken' => $access_token , 'details' => 1));
      //$result = AE_User::api()->member( array('id' => $ae_user->data->ID ), FALSE );
      if($result && !isset($result->error) ){ // && isset($result->data) && $result->data->AccessToken == $access_token
        $valid = TRUE;
        // Always use the user as returned from AE, to prevent client-side interception.
        // Such as changing a VerifiedEmail
        $ae_user = $result;
      }else{
        watchdog('ae_social_login', 'AE User accessToken validation failure.', NULL, WATCHDOG_WARNING);
      }
    }
    return $valid;
  }

  /**
   * Login and retrieve member details with an AE access token.
   *
   * @param $accessToken
   * @return mixed
   */
  static function auth($accessToken){
    return AE_User::api()->auth(array( 'accessToken' => $accessToken, 'details' => 1 ));
  }

  /**
   * Make an AE API request to reset a user's password
   *
   * @param $ae_user
   * @param $password
   * @return mixed
   */
  static function reset_password($ae_user, $password){
    foreach($ae_user->services as $ae_service){
      if($ae_service->Service == 'email'){
        $email = $ae_service->Username;
        $member_service_id =  $ae_service->ID;
        break;
      }
    }

    $result = NULL;
    if(isset($member_service_id) && isset($email)){
      $result = AE_User::api()->reset_password( array(
        'email' => $email,
        'password' => $password,
        'member_service_id' => $member_service_id,
      ));
    }
    return $result;
  }

  static function verify_email($email, $ae_user){
    $result = NULL;
    $email = strtolower($email);
    foreach($ae_user->services as $ae_service){
      if( !isset($ae_service->VerifiedEmail) ){
        //Verify each service with this email address
        $result = AE_User::api()->member_verify( array(
          'service' => $ae_service->Service,
          'verified_email' => $email,
          'id' => $ae_user->data->ID,
        ));
      }else{
        // If the service email matches then the user is already verified.
        $service_email = strtolower($ae_service->VerifiedEmail);
        if($service_email == $email){
          $result = $ae_user;
        }
      }
    }
    return $result;
  }

  /**
   * Insert a record of this request to verify an ae_user and account.
   *
   * @param $account
   * @param $ae_user
   */
  static function set_pending_verification($ae_user, $account){
    //$timestamp = REQUEST_TIME;
    //$token =  ae_social_login_rehash($timestamp, $ae_user);

    db_insert('ae_unverified_users')
      ->fields(array(
        'ae_uid' => $ae_user->data->ID,
        'uid' => $account->uid,
        'created' => REQUEST_TIME,
      ))
      ->execute();
  }

  /**
   * Get user uid from ae_unverified_users table for an ae_user.
   *
   * @param $ae_user
   * @param null $user
   * @return array
   */
  static function get_pending_verification($ae_user = NULL, $user = NULL){
    $verified = &drupal_static(__FUNCTION__, array(NULL, NULL));
    if($ae_user == NULL && $user == NULL){ return $verified; }
    if(!empty($verified)){ return $verified; }

    $query = db_select('ae_unverified_users', 'ae')
      ->fields('ae');

    if(isset($ae_user)){
      $query->condition('ae_uid', $ae_user->data->ID, '=');
    }
    if(isset($user)){
      $query->condition('uid', $user->uid, '=');
    }
    $result = $query->orderBy('created', 'DESC')
      ->execute()
      ->fetchAssoc();

    if(!empty($result)){
      $ae_uid = $result['ae_uid'];
      $uid = $result['uid'];
      $verified = array($ae_uid, $uid);
      db_delete('ae_unverified_users')
        ->condition('uid', $uid, '=')
        ->execute();
      return $verified;
    }
    return $verified;
  }

  /**
   * Check if a Drupal account is a verified match for an AE user account.
   *
   * @param $ae_user
   * @param null $account
   *   If no account is passed a search will be performed to find a matching account, which will be set to parameter reference.
   * @return bool
   */
  static function isVerified($ae_user, &$account){
    $isVerified = FALSE;
    $verified_accounts = &drupal_static(__FUNCTION__, array());
    if(!isset($account) && isset($verified_accounts[$ae_user->data->ID])){
      // load from static cache
      $account = &$verified_accounts[$ae_user->data->ID]['account'];
      return $verified_accounts[$ae_user->data->ID]['verified'];
    }

    // @deprecated
    $isVerified = AE_User::isJanrainVerified($ae_user, $account);

    // ### Email Verification
    $verified_emails = AE_User::get_verified_emails($ae_user) ?: array();
    $LOGINTOBOGGAN_VERIFY = module_exists('logintoboggan') && !variable_get('user_email_verification', TRUE);
    $validating_id = module_exists('logintoboggan') ? logintoboggan_validating_id() : NULL;

    if(isset($account)){
      if($LOGINTOBOGGAN_VERIFY && !isset($account->roles[$validating_id])){
        if(isset($AE_User) && $AE_User->uid == $account->uid){
          // The Drupal account is verified and linked to an AE account.
          $isVerified = TRUE;
        }else if(in_array(strtolower($account->mail), $verified_emails)){
          // The ae account is verified and matches the Drupal account.
          $isVerified = TRUE;
        }
      }
    }else{
      if(!empty($verified_emails)){
        // #### AE User has verified emails
        // Check Drupal accounts with emails matching $verified_emails
        $query = new EntityFieldQuery();
        $result = $query->entityCondition('entity_type', 'user')
          ->propertyCondition('mail', $verified_emails, 'IN')
          ->execute();
        if(isset($result['user'])){
          $users_ids = array_keys($result['user']);
          foreach($users_ids as $uid){
            $account = user_load($uid);
            // Is the account with this email verified?
            if($LOGINTOBOGGAN_VERIFY && !isset($account->roles[$validating_id]) ){
              $isVerified = TRUE;
              break;
            }
            drupal_alter('ae_user_isVerified_account', $account, $isVerified);
          }
        }
      }else if(isset($ae_user->data->Email)){
        // #### AE User has no verified emails
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'user')
          ->propertyCondition('mail', $ae_user->data->Email);
        $result = $query->execute();
        if(!empty($result['user'])){
          // We've found an account matching this unverified user,
          $uid = current(array_keys($result['user']));
          $account = user_load($uid);
          // but it's still not verified.
        }
      }
    }

    // allow additional account verification integrations
    if(!$isVerified){
      $context = array(
        'account' => &$account,
        'verified' => &$isVerified,
      );
      drupal_alter('ae_user_isVerified', $ae_user, $context);
    }
    $verified_accounts[$ae_user->data->ID]['account'] = $account;
    $verified_accounts[$ae_user->data->ID]['verified'] = $isVerified;
    return $isVerified;
  }

  private static function isJanrainVerified($ae_user, &$account){
    $isVerified = FALSE;
    $uuid = NULL;
    // Check the user services for a Janrain service.
    foreach($ae_user->services as $service){
      if(isset($service->PartnerCode) && $service->PartnerCode == 'janrain' && isset($service->PartnerID)){
        $uuid = $service->PartnerID;
      }
    }
    if( isset($uuid) ){
      // Check for Janrain uuid
      if( isset($account)){
        if(isset($account->field_janrain_capture_uuid[LANGUAGE_NONE][0]['value']) &&
          $account->field_janrain_capture_uuid[LANGUAGE_NONE][0]['value'] == $ae_user->data->Extended->uuid ){
          $isVerified = TRUE;
        }
      }else{
        $user_fields = field_info_instances('user', 'user');
        if(isset($user_fields['field_janrain_capture_uuid']) ){
          if(!isset($account)){
            $uuid = $ae_user->data->Extended->uuid;
            $query = new EntityFieldQuery();
            $query->entityCondition('entity_type', 'user')
              ->fieldCondition('field_janrain_capture_uuid', 'value', $uuid);
            $result = $query->execute();
            $users_ids = array_keys($result['user']);
            if(!empty($users_ids)){
              $account = user_load($users_ids[0]);
              $isVerified = TRUE;
            }
          }
        }
      }
    }
    return $isVerified;
  }

  private static function isLoginTobogganVerified($ae_user, &$account, &$isVerified){

  }

  /**
   * Make an AE API request to update AE user data attributes based on their corresponding values in the local Drupal user and profile2.
   *
   * @param $account
   * @param string $entityType
   * @return mixed|null
   */
  static function update($account, $entityType = 'user'){
    $ae_user = NULL;
    $AE_User = AE_User::load($account->uid, TRUE);
    if(isset($AE_User)){
      $ae_user = $AE_User->ae_data;
    }

    if(!isset($ae_user)){ return; }
    $save_AE_User = FALSE;

    // ### Update the AE User verification
    if(AE_User::isVerified($ae_user, $account, $AE_User)){
      // If the ae_user is a verified match for the Drupal account link the accounts.
      if(empty($ae_user->data->VerifiedEmail)){
        // If the ae_user isn't verified, see if we can verify them for $account->mail b/c they are now verified.
        $result = AE_User::verify_email($account->mail, $ae_user);
        if(isset($result->data)){
          // update the local entity
          $ae_user->data->VerifiedEmail = $result->data->VerifiedEmail;
          $AE_User->verified_email = $ae_user->data->VerifiedEmail;
          $AE_User->ae_data = $ae_user;
          $save_AE_User = TRUE;
        }
      }
    }
    drupal_alter('ae_user_update', $account, $ae_user);

    // ### Update AE User fields
    $mappings = variable_get('ae_field_mapping');
    //We don't update these remote fields in the AE user.
    $remote_fields = array('avatarurl', 'username');
    drupal_alter('ae_remote_fields', $remote_fields);
    $remote_fields = array_unique($remote_fields);

    $data = array();
    //### Update user / profile fields
    foreach($mappings as $ae_field => $drupal_field_path){
      if(in_array($ae_field, $remote_fields)){
        continue;
      }else if($ae_field == 'country'){
        $ae_field = 'countrycode';
      }

      // #### Get the local field
      $profile_type = NULL;
      $drupal_field_parts = explode('.', $drupal_field_path);
      $drupal_field_entityType = $drupal_field_parts[0];
      $field_name = ($drupal_field_entityType == 'profile2') ? $drupal_field_parts[2] : $drupal_field_parts[1];
      if($drupal_field_entityType == 'profile2'){
        $profile_type = 'profile_' . $drupal_field_parts[1];
      }
      if(isset($profile_type)){
        // from a profile,
        $profile = $account->{$profile_type};
        if(is_array($profile)){
          $drupal_field = isset($profile[$field_name]) ? $profile[$field_name] : NULL;
        }else if(is_object($profile)){
          $drupal_field = isset($profile->{$field_name}) ? $profile->{$field_name} : NULL;
        }
      }else{
        // or from the user.
        $drupal_field = isset($account->{$field_name}) ? $account->{$field_name} : NULL;
      }

      // #### Get the local field value.
      $field_info = field_info_field($field_name);
      $drupal_field_value = NULL;
      if(isset($drupal_field[LANGUAGE_NONE][0]['value'])){
        $drupal_field_value = $drupal_field[LANGUAGE_NONE][0]['value'];
        if($ae_field == 'birthdate' && !empty($drupal_field_value)){
          // Remove the time from birthdates.
          $drupal_field_value = explode(' ', $drupal_field_value);
          $drupal_field_value = $drupal_field_value[0];
        }
      }else if($field_info['type'] == 'addressfield'){
        // If this is an address field
        $address_field = AE_User::map_addressfield($ae_field);
        if(isset($address_field) && !empty($drupal_field[LANGUAGE_NONE][0][$address_field])){
          $drupal_field_value = $drupal_field[LANGUAGE_NONE][0][$address_field];
        }
      }

      // Check for user data update.
      if(!empty($drupal_field_value)){
        if( isset($ae_user->data->{AE_User::$DataFields[$ae_field]}) && $drupal_field_value == $ae_user->data->{AE_User::$DataFields[$ae_field]}){
          // No change, skip this field.
          continue;
        }
        // Update data for the AE User field request.
        $data[$ae_field] = $drupal_field_value;
        if($ae_field == 'countrycode'){
          // Drupal stores countrycode only, get the country name too.
          include_once DRUPAL_ROOT . '/includes/locale.inc';
          $countries = country_get_list();
          if(isset($countries[$drupal_field_value])){
            // Set the country name.
            $data['country'] = $countries[$drupal_field_value];
          }
        }
        if(isset($AE_User->{$ae_field})){
          // Update the local AE user record.
          $AE_User->{$ae_field} = $drupal_field_value;
        }
      }
    }

    //### Update AE User fields
    if(strtolower($account->mail) != strtolower($ae_user->data->Email)){
      $data['Email'] = $account->mail;
    }

    if(!empty($data)){
      // If AE user fields were updated
      //TODO this shouldn't be necessary...
      $data['username'] = $ae_user->data->Username;
      $data['id'] = $ae_user->data->ID;

      $result = AE_USER::api()->member_update($data);
      if(isset($result->data)){
        $ae_data = $result;
        $AE_User->ae_data = $ae_data;
        $save_AE_User = TRUE;
      }else if(isset($result->error)){
        watchdog('ae_social_login', 'There was an error updating the AE user record: %message', array( '%message' => $result->error->message));
      } else {
        watchdog('ae_social_login', 'There was an error updating the AE user record');
      }
    }

    if($save_AE_User){
      $AE_User->save(FALSE);
      return $AE_User->ae_data;
    }

    return NULL;
  }

  /**
   * Update the values of the AE_User entity.
   * This function doesn't try and change the user mapping, etc.
   *
   * @param $ae_user
   */
  public function updateAEUser($ae_user){
    if($this->ae_uid == $ae_user->data->ID){
      $this->ae_data = $ae_user;
      if(isset($ae_user->data->FirstName) && $ae_user->data->FirstName != $this->firstname){
        $this->firstname = $ae_user->data->FirstName;
      }
      if(isset($ae_user->data->Surname) && $ae_user->data->Surname != $this->surname){
        $this->surname = $ae_user->data->Surname;
      }
      if(isset($ae_user->data->VerifiedEmail) && $ae_user->data->VerifiedEmail != $this->verified_email){
        $this->verified_email = $ae_user->data->VerifiedEmail;
      }
    }
  }


  /**
   * Overrides Entity save()
   */
  public function save($account = NULL, $update = TRUE) {
    // Delete all other ae_user records for this user.
    db_delete('ae_users')
      ->condition('uid', $this->uid)
      ->condition('ae_uid', $this->ae_uid, '!=')
      ->execute();
    return entity_get_controller($this->entityType)->save($this, $account, $update);
  }

  /**
   * Register an AE user with the local site or join with an existing account
   *
   * @param $ae_user
   * @return array
   */
  static function register($ae_user){
    $account = NULL;
    if(!AE_User::isVerified($ae_user, $account)){
      //### Create a new Drupal user
      $email = AE_User::get_email($ae_user);
      $account = array();
      $account['name'] = $ae_user->data->Username;
      $account['mail'] = $email;
      $account['init'] = $email;
      $account['status'] = variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) == USER_REGISTER_VISITORS;
      $account['timezone'] = variable_get('date_default_timezone', 0);
      $account = user_save(drupal_anonymous_user(), $account);
    }
    // Return the existing or new user account.
    return $account;
  }

  /**
   * Register an Appreciation Engine user based on a Drupal account & password.
   *
   * @param $account
   * @param $password
   * @return AE_User
   */
  static function register_account($account, $password){
    if(!isset($account->ae_user) && !empty($password)){
      global $base_url;
      $domain = explode('://', $base_url);
      if(count($domain) > 1){
        $domain = $domain[1];
      }else{
        $domain = $base_url;
      }

      $params = array(
        'service' => 'email',
        'email' => $account->mail,
        'password' => $password,
        'username' => $account->name,
        'domain' => $domain,
      );

      $result = AE_User::api()->member_register($params);
      if(isset($result->data)){
        // Save the new or validated by email / pass user
        $ae_user = $result;
        $AE_User = new AE_User($ae_user, $account);
        $AE_User->save();
        return $AE_User;
      }else if(isset($result->error)){
        watchdog('ae_social_login', 'There was an error registering the user for account (%uid): %message', array(
          '%uid' => $account->uid, '%message' => $result->error->message,
        ));
      }
    }
  }

  /**
   * Copy field values from the AE user to local user & profile2
   *
   * @param $ae_user
   * @param $account
   */
  static function map_fields($ae_user, $account){
    $field_mapping = variable_get('ae_field_mapping', array());
    $drupal_map = array(
      'profile2' => array(
        'profiles' => array(),
        'updated' => array(),
        'new' => array(),
      ),
      'user' => array(
        'updated' => FALSE,
      ),
    );

    $local_fields = array();
    // TODO we may want to handle these local overrides better.
    drupal_alter('ae_user_local_fields', $local_fields);
    $local_fields = array_unique($local_fields);
    $entity = NULL;

    $updated_fields = array();
    foreach($field_mapping as $ae_field => $drupal_field_path){
      // ### Get the AE field value.
      if($ae_field == 'country'){
        $ae_field = 'countrycode';
      }
      $drupal_field_parts = explode('.', $drupal_field_path);
      $ae_dataField = isset(AE_User::$DataFields[$ae_field]) ? AE_User::$DataFields[$ae_field] : NULL;
      if($ae_dataField == NULL){ continue; }

      $ae_field_value = NULL;
      if($ae_field === 'avatarurl'){
        // create a local file or use an existing one.
        $ae_field_value = (array) file_uri_to_object( 'appreciation-engine://avatar/'. $ae_user->data->ID . '.jpg', TRUE );
      }else{
        $ae_field_value = isset($ae_user->data->{$ae_dataField}) ? $ae_user->data->{$ae_dataField} : NULL;
      }

      if(empty($ae_field_value)){ continue; }

      unset($entityUpdated);
      $entity_type = NULL;
      $bundle = NULL;
      if($drupal_field_parts[0] == 'profile2'){
        // ### Map AE field to a Profile field.
        $entity_type = 'profile2';
        $bundle = $profile_type_name = $drupal_field_parts[1];
        $field_name = $drupal_field_parts[2];
        $entityUpdated = &$drupal_map['profile2']['updated'][$profile_type_name];
        if(!isset($drupal_map['profile2']['profiles'][$profile_type_name])){
          // Load the user's profile
          $profile = profile2_load_by_user($account, $profile_type_name);
          if($profile == false){
            // or create a new one.
            $profile = profile2_create(array('type' => $profile_type_name, 'uid' => $account->uid));
            $drupal_map['profile2']['new'][$profile_type_name] = TRUE;
            //$entityUpdated = TRUE;
          }
          $drupal_map['profile2']['profiles'][$profile_type_name] = $profile;
          $entityUpdated = FALSE;
        }
        $entity = &$drupal_map['profile2']['profiles'][$profile_type_name];
      }else if($drupal_field_parts[0] == 'user'){
        // ### Map AE field to a user field.
        $entity_type = $bundle = 'user';
        $entity = &$account;
        $entityUpdated = &$drupal_map['updated'];
      }

      if( $entity ){
        // ### Retrieve the field value.
        $field_info = field_info_field($field_name);
        $field_info_instance = field_info_instance($entity_type, $field_name, $bundle);
        if(!isset($field_info) || !isset($field_info_instance)){ continue; }

        // Add the field if not present.
        if(!isset($entity->{$field_name})){ $entity->{$field_name} = array( LANGUAGE_NONE => array( 0 => array() ) ); }
        $entity_field_item = &$entity->{$field_name}[LANGUAGE_NONE][0];

        $value_field = 'value';
        if($field_info['type'] == 'addressfield'){
          $value_field = AE_User::map_addressfield($ae_field);
        }
        else if($field_info['type'] == 'geofield'){
          $value_field = AE_User::map_geofield($ae_field);
        } else if($ae_field === 'avatarurl'){
          $value_field = 'uri';
        }
        if(!isset($value_field)){
          // This field isn't defined, bail.
          continue;
        } else if(isset($entity_field_item) && !array_key_exists($value_field, $entity_field_item)){
          // Add a default value if empty.
          $entity_field_item[$value_field] = NULL;
        }
        $EMPTY = empty($entity_field_item[$value_field]);
        $UPDATE = !in_array($field_name, $local_fields);

        $entity_field_value = $entity_field_item[$value_field];
        if(isset($entity_field_item['date_type']) && !$EMPTY){
          $date_parts = explode(' ', $entity_field_value);
          if(count($date_parts) > 1){
            $entity_field_value = $date_parts[0];
          }
        }

        // ### Set or update the field value.
        if($EMPTY || $UPDATE && $entity_field_value != $ae_field_value){
          $entity_field_item[$value_field] = $ae_field_value;
          $entityUpdated = TRUE;
          $updated_fields[$field_name] = array('info' => $field_info, 'item' => &$entity_field_item);
        }
      }
    }

    foreach($updated_fields as $field_name => $field_data){
      $field_info = $field_data['info'];
      $entity_field_item = $field_data['item'];
      if($field_info['type'] == 'geofield'){
        $entity_field_item = geofield_compute_values($entity_field_item);
      }
    }

    // ### Save The Profile
    foreach($drupal_map['profile2']['profiles'] as $profile_type => $profile){
      if(isset($drupal_map['profile2']['new'][$profile_type])){
        profile2_save($profile);
      }
    }
    return array( 'user' => $drupal_map['user']['updated'], 'profile2' => $drupal_map['profile2']['updated']);
  }

  static function hasPartnerCode($ae_user, $partnerCode){
    foreach($ae_user->services as $ae_service){
      if(isset($ae_service->partnerCode) && $ae_service->partnerCode == $partnerCode){
        return TRUE;
      }
    }
    return FALSE;
  }

  static function get_email($ae_user){
    if(!is_object($ae_user)) $ae_user = (object) $ae_user;
    $email = isset($ae_user->data->VerifiedEmail) ? $ae_user->data->VerifiedEmail : null;
    if(!isset($email) && isset($ae_user->data->Email)){
      $email = $ae_user->data->Email;
    }

    if(!isset($email)) {
      foreach($ae_user->services as $service){
        if( !$email && isset($service->VerifiedEmail) && $service->VerifiedEmail != "null" ){
          //gather most popular verified email
          isset($user_emails[$service->VerifiedEmail]) ? $user_emails[$service->VerifiedEmail]++ : $user_emails[$service->VerifiedEmail] = 1;
        }
      }
      if(count($user_emails) > 0){
        //if no verified email on user, get most popular from services
        arsort($user_emails);//sort by most popular
        $email = reset(array_flip($user_emails)); //grab top email
      }
    }

    return $email;
  }

  static function get_verified_emails($ae_user){
    $verified_emails = array();
    if(isset($ae_user->data->VerifiedEmail)){
      $verified_emails[] = strtolower($ae_user->data->VerifiedEmail);
    }
    foreach($ae_user->services as $service){
      if(!empty($service->VerifiedEmail)){
        $verified_emails[] = strtolower($service->VerifiedEmail);
      }
    }
    return array_values(array_unique($verified_emails));
  }

  static function get_service($ae_user, $service_type){
    $service = NULL;
    foreach($ae_user->services as $ae_service){
      if($ae_service->Service == $service_type){
        $service = $ae_service;
        break;
      }
    }
    return $service;
  }

  private static function map_addressfield($ae_field){
    $address_field = NULL;
    switch($ae_field){
      case 'address':
        $address_field = 'thoroughfare';
        break;
      case 'addressline2':
        $address_field = 'premise';
        break;
      case 'city':
        $address_field = 'locality';
        break;
      case 'state':
        $address_field = 'administrative_area';
        break;
      case 'postcode':
        $address_field = 'postal_code';
        break;
      case 'country':
      case 'countrycode':
      case 'geocountry':
        $address_field = 'country';
        break;
    }
    return $address_field;
  }

  private static function map_geofield($ae_field){
    switch($ae_field){
      case 'geolat':
        return 'lat';
      case 'geolong':
        return 'lon';
    }
    return NULL;
  }

}

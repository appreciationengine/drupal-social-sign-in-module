<?php
namespace Appreciation_Engine;

class AESettings {

  static $extra_fields = array(
    'firstname',
    'surname',
    'email',
    'password',
    'address',
    'addressline2',
    'city',
    'state',
    'country',
    'postcode',
    'homephone',
    'mobilephone',
    'username',
    'website',
    'bio',
    'gender',
    'birthdate',
    'avatarurl',
    'geocountry',
    'geolat',
    'geolong',
  );

  static $extra_info = array(
    'global',
    'login',
    'register',
    'required',
    'recover_password',
    'reset_password',
    'reset_password_ok',
    'verify_email',
    'verify_email_ok',
  );

  static $languages = array(
    'en_US' => 'English',
    'sq_AL' => 'Albanian',
    'ar_MA' => 'Arabic',
    'eu_ES' => 'Basque',
    'be_FR' => 'Belgium (French)',
    'be_NL' => 'Belgium (Netherland)',
    'bg_BG' => 'Bulgarian',
    'ca_ES' => 'Catalan',
    'es_CL' => 'Chilean Spanish',
    'zh_CN' => 'Chinese',
    'cs_CZ' => 'Czech',
    'da_DK' => 'Danish',
    'nl_NL' => 'Dutch (Netherland)',
    'fi_FI' => 'Finnish',
    'fr_FR' => 'French',
    'de_DE' => 'German',
    'gr_EL' => 'Greek',
    'he_IL' => 'Hebrew',
    'hi_IN' => 'Hindi',
    'hu_HU' => 'Hungarian',
    'id_ID' => 'Indonesian',
    'it_IT' => 'Italian',
    'ja_JP' => 'Japanese',
    'no_NO' => 'Norwegian',
    'fa_IR' => 'Persian (Farsi)',
    'pl_PL' => 'Polish',
    'pt_BR' => 'Portuguese (Brazil)',
    'pt_PT' => 'Portuguese (Portugal)',
    'ro_RO' => 'Romanian',
    'ru_RU' => 'Russian',
    'sr_RS' => 'Serbian',
    'sk_SK' => 'Slovak',
    'es_ES' => 'Spanish',
    'sv_SE' => 'Swedish',
    'zh_TW' => 'Taiwanese',
    'th_TH' => 'Thai',
    'tr_TR' => 'Turkish',
    'ua_UA' => 'Ukrainian',
    'vi_VN' => 'Vietnamese'
  );

  static $flow_text = array(
    'error_header',
    'login_header',
    'register_header',
    'add_info_header',
    'add_info_button',
    'reset_pw_header',
    'reset_pw_sent',
    'reset_pw_instructions',
    'reset_pw_button',
    'reset_pw_confirm_header',
    'reset_pw_confirm_button',
    'reset_pw_done_header',
    'reset_pw_done_message',
    'reset_pw_done_button',
    'reset_pw_confirm_instructions',
    'verify_email_header',
    'verify_email_sent',
    'verify_email_instructions',
    'verify_email_retry_button',
    'verify_email_success_header',
    'verify_email_success_message',
    'verify_email_success_button',
    'verify_email_error_header',
    'verify_email_error_message',
    'forgot_password_link',
    'recover_password_link',
    'have_account_link ',
    'need_help_link',
    'create_account_link'
  );

  /**
   * @param bool $enabled_only
   * @return array
   *     service names of the enabled services
   */
  public static function services($enabled_only = TRUE){
    $ae_exposed_services = (array) variable_get('ae_exposed_services', []);
    if($enabled_only){ $ae_exposed_services = array_filter($ae_exposed_services); }
    return array_keys($ae_exposed_services);
  }

  /**
   * @param bool $enabled_only
   * @return array
   *     services for use in a for #options element.
   */
  public static function services_options($enabled_only = TRUE){
    $service_options = [];
    $ae_exposed_services = variable_get('ae_exposed_services', []);
    foreach($ae_exposed_services as $service_name => $enabled){
      if($enabled_only && $service_name !== $enabled){ continue; }
      $service_options[$service_name] = ucwords($service_name);
    }

    return $service_options;
  }

}

<?php

/**
 * Build the register link markup.
 * @param       $social
 * @param array $options
 * @return string
 */
function ae_social_login_registerLink($social, $options = []){
  $options['social'] = $social;
  return theme('ae_register_link', $options);
}

function theme_ae_register_link($vars){
  $social = isset($vars['social'])? $vars['social'] : '';
  $Social = ($social == 'youtube') ? 'YouTube' : ucwords($social);

  $attributes = isset($vars['attributes']) ? $vars['attributes'] : [];
  $remote_only = isset($vars['remote_only']) ? $vars['remote_only'] : FALSE;
  $type = isset($vars['type']) ? $vars['type'] : 'register';
  if($remote_only){
    $attributes['data-remote-only'] = NULL;
  }

  $link_text = isset($vars['label']) ? $vars['label'] : '';
  if(empty($link_text)){
    $action = isset($vars['action']) ? $vars['action'] : 'Login';
    $preposition = isset($vars['preposition']) ? $vars['action'] : 'with';
    $link_text = "<span class=\"auth-action\">$action</span> <span class=\"preposition\">$preposition</span> <span class=\"social\">$Social</span>";
  }
  $link_text = "<i class=\"icon icon-{$social}\"></i><span class=\"label\">$link_text</span>";

  $attributes['title'] = $Social;
  $attributes["data-ae-$type-link"] = $social;
  $attributes['class']  = isset($attributes['class']) ? $attributes['class'] : [];
  $attributes['class'] = array_merge( $attributes['class'], ['ae-register-link', 'ae-social-login-service', $social]);

  if(!isset($attributes['id'])){
    $attributes['id'] = drupal_html_id('ae-register-link');
  }

  $link_options = [
    'attributes' => $attributes,
    'html' => TRUE,
    'fragment' => "$social-login",
    'external' => TRUE,
  ];

  return l($link_text, '', $link_options);
}

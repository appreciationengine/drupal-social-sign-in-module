<?php

/**
 * Implements MODULE_NAME_token_info()
 */
function ae_social_login_token_info() {
  $info = array();

  $info['types']['ae'] = array(
    'name' => t('ae_social_login Tokens'),
    'description' => t('Base token type for ae_social_login tokens'),
  );

  // ## Define Token instances
  $info['tokens']['ae'] = array(
    'register-link' => array(
      'name' => t('Register Link'),
      'description' => t('A data-ae-register-link'),
      'dynamic' => TRUE,
    ),
    'login-link' => array(
      'name' => t('Login Link'),
      'description' => t('A data-ae-login-link'),
      'dynamic' => TRUE,
    ),
    'auth-link' => array(
      'name' => t('Auth Link'),
      'description' => t('A data-ae-auth-link'),
      'dynamic' => TRUE,
    ),
  );

  return $info;
}

/**
 * Implements MODULE_NAME_tokens()
 */
function ae_social_login_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);

  if ($type == 'ae') {
    foreach ($tokens as $name => $original) {
      if(strpos($name, 'register-link') === 0){
        $replacements[$original] = ae_social_login_tokens_registerLink($name, $data, $options);
      }else if(strpos($name, 'login-link') === 0){
        $options['type'] = 'login';
        $replacements[$original] = ae_social_login_tokens_registerLink($name, $data, $options);
      }else if(strpos($name, 'auth-link') === 0){
        $options['type'] = 'auth';
        $replacements[$original] = ae_social_login_tokens_registerLink($name, $data, $options);
      }
    }
  }

  return $replacements;
}

/**
 * Replace tokens for Appreciation Engine register links [ae:register-link:<social>]
 *
 * @param       $token
 * @param array $data
 * @param array $options
 * @return string
 */
function ae_social_login_tokens_registerLink($token, array $data = array(), array $options = array()){
  $replacement = '';

  $tokens = explode(":", $token);
  if(count($tokens) < 2 ){ return ''; }
  $social = strtolower($tokens[1]);
  $options['remote_only'] = (count($tokens) > 2) ? $tokens[2] === 'remote' : FALSE;
  $services = variable_get('ae_available_services', array());
  foreach($services as $service => $enabled){
    if($service == $social){
      // Only create links for services that exist on the AE app.
      $options['social'] = $social;
      $replacement = theme('ae_register_link', $options);
      break;
    }
  }

  return $replacement;
}

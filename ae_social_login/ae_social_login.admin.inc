<?php
module_load_include('inc', 'ae_social_login', 'AESettings');
use Appreciation_Engine\AESettings;


function ae_social_login_form_appreciation_engine_basic_settings_form_alter(&$form, &$form_state) {

  $form['appreciation_engine_api_key']['#description'] .=  t("<br/>Once you have entered the api key above, you can simply place the Appreciation Engine Social Login block on a page for easy deployment. For further customisation and options see the <a href='admin/config/content/ae_social_login/widget_settings'>widget</a> & <a href='admin/config/content/ae_social_login/data_business'>data</a> settings tabs.");

  $form['ae_return_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Return URL (optional)'),
    '#default_value' => variable_get('ae_return_url', null),
    '#description' => t('Default is the current page that the widget appears on.'),
  );

  $form['#submit'][] = 'ae_social_login_basic_settings_form_submit';
}

/**
 * Update the return url in the ae_settings_master variable
 *
 * @param $form
 * @param $form_state
 */
function ae_social_login_basic_settings_form_submit($form, &$form_state){
  $ae_settings = variable_get('ae_settings_master') ? variable_get('ae_settings_master') : array();
  if(variable_get('ae_return_url')){
    $ae_settings['return_url'] = variable_get('ae_return_url');
  } else {
    unset($ae_settings['return_url']);
  }
  variable_set('ae_settings_master', $ae_settings);
}

/**
 * @param $form
 * @param $form_state
 * @return mixed
 */
function ae_social_login_widget_settings_services_form($form, &$form_state) {
  $services = variable_get('ae_available_services', array());
  $exposed_services = variable_get('ae_exposed_services', array());
  $service_options = array();
  foreach($services as $service_name => $enabled){
    $service_options[$service_name] = t(ucwords($service_name));
  }
  $form['ae_available_services'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Available Services'),
    '#description' => t('Choose which can be used for social sign in. Leave blank for all services; otherwise, <em>login will be restricted to selected services</em>.'),
    '#options' => $service_options,
    '#default_value' => $services,
  );
  $form['ae_exposed_services'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Exposed Services'),
    '#description' => t('Choose which services should be displayed on forms; defaults to all available services.'),
    '#options' => $service_options,
    '#default_value' => $exposed_services,
  );

  if(empty($services)){
    $form['ae_available_services']['#description'] = t('All of the services you have added in your Appreciation Engine dashboard will appear here once you have entered your AE API Key');
  }

  $module_path = drupal_get_path('module', 'ae_social_login');
  drupal_add_css($module_path.'/css/ae_social_login.admin.css', array('type' => 'file'));

  $form['#submit'][] = 'ae_social_login_widget_settings_services_form_submit';
  $form = system_settings_form($form);
  return $form;
}

function ae_social_login_widget_settings_services_form_submit($form, &$form_state) {
  $ae_settings = variable_get('ae_settings_master', array());
  $ae_settings_services = array();
  $ae_available_services = $form_state['values']['ae_available_services'];
  foreach($ae_available_services as $service => $enabled){
    if($enabled !== 0){
      $ae_settings_services[] = $service;
    }
  }
  if(!empty($ae_settings_services)){
    $ae_exposed_services = $form_state['values']['ae_exposed_services'];
    $ae_exposed_services = array_intersect($ae_exposed_services, $ae_available_services);
    $form_state['values']['ae_exposed_services'] = array_combine($ae_exposed_services, $ae_exposed_services);
  }

  $ae_settings['services'] = implode(",", $ae_settings_services);
  if(empty($ae_settings['services'])){
    $ae_settings['services'] = NULL;
  }
  variable_set('ae_settings_master', $ae_settings);
}

function ae_social_login_widget_settings_user_messaging_form($form, &$form_state) {
  //## USER MESSAGING
  $form['ae_sign_in_call_to_action'] = array(
    '#type' => 'textfield',
    '#title' => t('Sign in Call to Action'),
    '#description' => t('Not used in embedded mode'),
    '#default_value' => variable_get('ae_sign_in_call_to_action', 'Sign in using one of the services below'),
  );
  $form['ae_logout_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Logout Link'),
    '#default_value' => variable_get('ae_logout_link', 'Not {{Firstname}}?'),
  );
  return system_settings_form($form);
}

/**
 * @return mixed
 */
function ae_social_login_widget_settings_flow_and_behaviour_form(){
  //## FLOW / BEHAVIOUR

  //TODO remove variables, use $ae_settings
  $ae_settings = variable_get('ae_settings_master', array());

  $form['ae_flow_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Flow css'),
    '#default_value' =>  isset($ae_settings['flow_css']) ? $ae_settings['flow_css'] : null,
    '#description' => t('A Url to some custom css to add to your widget (eg. Twitter bootstrap)'),
  );
  $form['ae_auth_window'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Modal Popup'),
    '#default_value' => isset($ae_settings['auth_window']) ? $ae_settings['auth_window'] : false,
    '#description' => t('Embed the sign in widget directly in the page or show in modal'),
  );
  $form['ae_include_social'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show social icons'),
    '#default_value' => isset($ae_settings['include_social']) ? $ae_settings['include_social'] : true,
    '#description' => t('Show individual service icons. Turning this off will replace all with a "Register" button that handles the process off site.'),
  );
  $form['ae_single_sign_on'] = array(
    '#type' => 'checkbox',
    '#title' => t('Single Sign On'),
    '#default_value' => isset($ae_settings['sso']) ? $ae_settings['sso'] == 'application' : true,
    '#description' => t('User will automatically be logged into any other websites that have this widget and API Key present when they visit them'),
  );
  $form['ae_social_first'] = array(
    '#type' => 'checkbox',
    '#title' => t('Social First'),
    '#default_value' => isset($ae_settings['social_first']) ? $ae_settings['social_first'] : true,
    '#description' => t('Position social icons before email login or vice versa.'),
  );
  $form['ae_disable_extra_fields_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable Extra Fields Screen'),
    '#default_value' => isset($ae_settings['extra_fields_screen']) ? $ae_settings['extra_fields_screen'] == 'disabled' : false,
    '#description' => t('Turn off if you wish to add your own custom form to collect extra fields. You can map specific fields to their AE counterparts <a href="/admin/config/content/ae_social_login/data_business_rules/field_mapping">here</a>.'),
  );
  $form['ae_disable_error_message'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable Error message'),
    '#default_value' => isset($ae_settings['display_error_message']) ? !$ae_settings['display_error_message'] : false,
    '#description' => t('Turn off if you wish to add your own custom form error handling'),
  );
  $form['ae_verify_email_for_login'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verify Email For Login'),
    '#default_value' => isset($ae_settings['verify_email_for_login']) ? $ae_settings['verify_email_for_login'] : false,
    '#description' => t('Won\'t trigger login events unless the user has a verified email address'),
  );

  $form['ae_override_login'] = array(
    '#type' => 'checkbox',
    '#title' => t('Skip Drupal User Login'),
    '#default_value' => isset($ae_settings['ae_override_login']) ? $ae_settings['ae_override_login'] : 0,
    '#description' => t('Do not create a Drupal user after AE user login / registration. You can also use custom hooks on the AE JS API for custom registration integrations.'),
  );
  $form['ae_override_logout'] = array(
    '#type' => 'checkbox',
    '#title' => t('Attach to Drupal Logout Links'),
    '#default_value' => isset($ae_settings['override_logout']) ? $ae_settings['override_logout'] : true,
    '#description' => t('Add an Appreciation Engine logout that will log users out of both AE and Drupal. Note: disabling this will leave AE users logged in to the Appreciation Engine at all times.'),
  );

  $form['smartAuth'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Smart Auth'),
    '#default_value' => variable_get('ae_social_login_smartAuth', TRUE) ? 1 : 0,
    '#description' => t('Use custom logic to facilitate account merge via AE <em>auth</em> links after the first login / registration in a browser session.'),
  );

  $form['debug_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug Mode'),
    '#default_value' => variable_get('ae_social_login_debug_mode', FALSE) ? 1 : 0,
    '#description' => t('Enable additional debug messaging for development purposes.'),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['#submit'][] = 'ae_social_login_widget_settings_flow_and_behaviour_form_submit';
  return $form;
}

function ae_social_login_widget_settings_flow_and_behaviour_form_submit($form, &$form_state) {
  $ae_settings = variable_get('ae_settings_master', array());
  $values = $form_state['values'];

  foreach ($values as $form_field_name => $field) {
    $key = str_replace("ae_", "", $form_field_name);

    if($key == 'flow_css'){
      $ae_settings[$key] = check_url($values[$form_field_name]);
    }else if($key == 'single_sign_on'){
      // update sso setting
      $ae_settings['sso'] = $values[$form_field_name] == TRUE ? 'application' : 'local';
    }else if($key == 'disable_extra_fields_screen'){
      // update extra fields screen setting
      $ae_settings['extra_fields_screen'] = $values[$form_field_name] == TRUE ? 'disabled' : 'after';
    }else if($key == 'disable_error_message'){
      // update extra fields screen setting
      $ae_settings['display_error_message'] = !$values[$form_field_name];
    }else if($key !== $form_field_name){
      $ae_settings[$key] = $values[$form_field_name] == 1 ? TRUE : FALSE;
    }
  }

  variable_set('ae_settings_master', $ae_settings);
  variable_set('ae_social_login_smartAuth', $form_state['values']['smartAuth'] ? TRUE : FALSE);
  variable_set('ae_social_login_debug_mode', $form_state['values']['debug_mode'] ? TRUE : FALSE);
}

function ae_social_login_widget_settings_email_format_form(){
  //## Email Formatting

  $ae_settings = variable_get('ae_settings_master', array());
  $email_settings = isset($ae_settings['email_format']) ? $ae_settings['email_format'] : array();

  $form['ae_email_format_description'] = array(
    '#markup' => t('<div class="description">If you are using Appreciation Engine\'s verify email functionality, this section will allow you to tailor the emails that are sent to your users. All fields have a default setting in place, however these fields will allow you to override those defaults.</div>' ),
  );
  $form['ae_email_format_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background Colour'),
    '#default_value' => isset($email_settings['background_color']) ? $email_settings['background_color'] : null,
    '#description' => t('As hexidecimal value'),
  );
  $form['ae_email_format_font_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Size'),
    '#default_value' => isset($email_settings['font_size']) ? $email_settings['font_size'] : null,
    '#description' => t('In pixels (px)'),
  );
  $form['ae_email_format_font_family'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Family'),
    '#default_value' => isset($email_settings['font_family']) ? $email_settings['font_family'] : null,
  );
  $form['ae_email_format_font_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Color'),
    '#default_value' => isset($email_settings['font_color']) ? $email_settings['font_color'] : null,
    '#description' => t('As hexidecimal value'),
  );
  $form['ae_email_format_show_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Header'),
    '#default_value' => isset($email_settings['show_header']) ? $email_settings['show_header'] : false,
  );
  $form['ae_email_format_header_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Header Background Color'),
    '#default_value' => isset($email_settings['header_background_color']) ? $email_settings['header_background_color'] : null,
    '#description' => t('As hexidecimal value'),
  );
  $form['ae_email_format_header_font_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Header Font Color'),
    '#default_value' => isset($email_settings['header_font_color']) ? $email_settings['header_font_color'] : null,
    '#description' => t('As hexidecimal value'),
  );
  $form['ae_email_format_image_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Image URL'),
    '#default_value' => isset($email_settings['image_url']) ? $email_settings['image_url'] : null,
  );
  $form['ae_email_format_show_footer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Footer'),
    '#default_value' => isset($email_settings['show_footer']) ? $email_settings['show_footer'] : false,
  );
  $form['ae_email_format_footer_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Footer Background Color'),
    '#default_value' => isset($email_settings['footer_background_color']) ? $email_settings['footer_background_color'] : null,
    '#description' => t('As hexidecimal value'),
  );
  $form['ae_email_format_logo_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo Link'),
    '#default_value' => isset($email_settings['logo_link']) ? $email_settings['logo_link'] : null,
  );
  $form['ae_email_format_logo_image_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo Image URL'),
    '#default_value' => isset($email_settings['logo_image_url']) ? $email_settings['logo_image_url'] : null,
  );
  $form['ae_email_format_copyright'] = array(
    '#type' => 'textfield',
    '#title' => t('Copyright'),
    '#default_value' => isset($email_settings['copyright']) ? $email_settings['copyright'] : null,
  );
  $form['ae_email_format_reset_pw_email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Reset Password Email Subject'),
    '#default_value' => isset($email_settings['reset_pw_email_subject']) ? $email_settings['reset_pw_email_subject'] : null,
  );
  $form['ae_email_format_reset_pw_email_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Reset Password Email Message'),
    '#default_value' => isset($email_settings['reset_pw_email_message']) ? $email_settings['reset_pw_email_message'] : null,
  );
  $form['ae_email_format_reset_pw_email_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Reset Password Email Link'),
    '#default_value' => isset($email_settings['reset_pw_email_link']) ? $email_settings['reset_pw_email_link'] : null,
  );
  $form['ae_email_format_verify_email_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Verify Email Message'),
    '#default_value' => isset($email_settings['verify_email_message']) ? $email_settings['verify_email_message'] : null,
  );
  $form['ae_email_format_verify_email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Verify Email Subject'),
    '#default_value' => isset($email_settings['verify_email_subject']) ? $email_settings['verify_email_subject'] : null,
  );
  $form['ae_email_format_verify_email_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Verify Email Link'),
    '#default_value' => isset($email_settings['verify_email_link']) ? $email_settings['verify_email_link'] : null,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['#submit'][] = 'ae_social_login_widget_settings_email_format_form_submit';
  return $form;
}

function ae_social_login_widget_settings_email_format_form_submit($form, &$form_state) {
  $ae_settings = variable_get('ae_settings_master', array());
  $email_format = isset($ae_settings['email_format']) ? $ae_settings['email_format'] : array();
  $values = $form_state['values'];

  foreach ($values as $form_field_name => $field) {
    if(stristr($form_field_name, "ae_") && $field){
      $key = str_replace("ae_email_format_", "", $form_field_name);
      $email_format[$key] = check_url($field);
      if($field == 1) $email_format[$key] = "TRUE";
    } 
  }

  $ae_settings['email_format'] = $email_format;
  variable_set('ae_settings_master', $ae_settings);
}

function ae_social_login_data_business_rules_extra_fields_form($form, &$form_state) {
  //## Extra Fields

  $form['ae_required_fields_message'] = array(
    '#markup' => t("<div class=\"description\">Define fields you require to be collected. These will be collected pre or post social auth if not returned from the provider - this flow can be configured in the widget settings page. (NB: email and display name are always required).</div>"),
  );
  $form['ae_required_fields_required_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Required fields message'),
    '#default_value' => variable_get('ae_required_fields_required_message', "{{Fieldname}} is required"),
  );
  $form['ae_required_fields_error_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Error message'),
    '#default_value' => variable_get('ae_required_fields_error_message', "Please enter a valid {{Fieldname}}"),
  );

  $form['ae_language'] = array(
    '#type' => 'select',
    '#options' => AESettings::$languages,
    '#title' => t('Language'),
    '#default_value' => variable_get('ae_language', 'en_US'),
    '#description' => t('Language to use for error messages during extra fields capture'),
  );

  $ae_extra_fields = variable_get('ae_extra_fields', drupal_map_assoc(AESettings::$extra_fields));

  $extra_fields_options = array();
  foreach($ae_extra_fields as $extra_field => $enabled){
    $extra_fields_options[$extra_field] = t(ucwords($extra_field));
  }

  foreach($ae_extra_fields as $field){
    $form['ae_extra_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Required Fields'),
      '#options' => $extra_fields_options,
      '#default_value' => $ae_extra_fields,
    );
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['#submit'][] = 'ae_social_login_data_business_rules_extra_fields_form_submit';
  return $form;
}

function ae_social_login_data_business_rules_extra_fields_form_submit($form, &$form_state) {
  $ae_settings = variable_get('ae_settings_master', array());
  $ae_extra_fields = $form_state['values']['ae_extra_fields'];
  variable_set('ae_extra_fields', $ae_extra_fields);

  $ae_required_fields_required_message = $form_state['values']['ae_required_fields_required_message'];
  $ae_required_fields_error_message = $form_state['values']['ae_required_fields_error_message'];

  $extra_fields = array();
  foreach($ae_extra_fields as $extra_field => $required){
    if($required === $extra_field){
      $extra_fields[$extra_field]['required'] = TRUE;
      $extra_fields[$extra_field]['required_message'] = str_replace("{{Fieldname}}", ucfirst($extra_field), $ae_required_fields_required_message);
      $extra_fields[$extra_field]['error_message'] = str_replace("{{Fieldname}}", $extra_field, $ae_required_fields_error_message);
    }
  }

  $ae_settings['language'] = $form_state['values']['ae_language'];
  $ae_settings['extra_fields'] = $extra_fields;
  variable_set('ae_settings_master', $ae_settings);
}

function ae_social_login_data_business_rules_extra_info_form($form, &$form_state) {
  //## Extra Info
  $ae_settings = variable_get('ae_settings_master', array());
  $extra_info_values = isset($ae_settings['extra_info']) ? $ae_settings['extra_info'] : array();

  $form['ae_message_extra_info'] = array(
    '#markup' => t('<div class="description" >Optional custom text that can be added at various points and positions during the auth flow. Fields are named after the place in the flow where they will appear.</div>'),
  );
  $extra_info_fields = AESettings::$extra_info;

  foreach($extra_info_fields as $key => $field){

    $title = _ae_social_login_get_title_from_field($field);
    $position = isset($extra_info_values[$field]) ? key($extra_info_values[$field]) : 'top';
    $form['ae_extra_info_'.$field.'_heading'] = array(
      '#type' => 'textfield',
      '#title' => t($title.' Heading'),
      '#default_value' => isset($extra_info_values[$field][$position]['title']) ? $extra_info_values[$field][$position]['title'] : null,
    );
    $form['ae_extra_info_'.$field.'_text'] = array(
      '#type' => 'textarea',
      '#title' => t($title.' Text'),
      '#default_value' => isset($extra_info_values[$field][$position]['text']) ? $extra_info_values[$field][$position]['text'] : null,
    );
    $form['ae_extra_info_'.$field.'_position'] = array(
      '#type' => 'select',
      '#title' => t($title.' Position'),
      '#options' => array('top' => 'Top', 'bottom' => 'Bottom'),
      '#default_value' => $position,
    );
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['#submit'][] = 'ae_social_login_data_business_rules_extra_info_form_submit';
  return $form;
}

function ae_social_login_data_business_rules_extra_info_form_submit($form, &$form_state) {
  $ae_settings = variable_get('ae_settings_master', array());
  $extra_info = isset($ae_settings['extra_info']) ? $ae_settings['extra_info'] : array();
  $values = $form_state['values'];

  foreach ($values as $form_field_name => $field) {
    if(stristr($form_field_name, "_text") && $field){
      $key = str_replace("ae_extra_info_", "", $form_field_name);
      $key = str_replace("_text", "", $key);
      $heading = check_url($values["ae_extra_info_".$key."_heading"]);
      $text = check_url($field);
      $position = check_url($values["ae_extra_info_".$key."_position"]);
      $extra_info[$key] = array($position => array('text' => $text));
      if($heading) $extra_info[$key][$position]['title'] = $heading;
    } 
  }

  $ae_settings['extra_info'] = $extra_info;
  variable_set('ae_settings_master', $ae_settings);
}

function ae_social_login_data_business_rules_flow_text_form($form, &$form_state) {
  //## FLOW TEXT

  $ae_settings = variable_get('ae_settings_master', array());
  $flow_text_values = isset($ae_settings['flow_text']) ? $ae_settings['flow_text'] : array();

  $form['ae_message_flow_text'] = array(
    '#markup' => t('<div class="description">Optional custom text that can be added at various points and positions during the auth flow. Fields are named after the place in the flow where they will appear.</div>'),
  );

  $flow_text_fields = AESettings::$flow_text;
  foreach($flow_text_fields as $field){
    $title = _ae_social_login_get_title_from_field($field);
    $form['ae_flow_text_'.$field] = array(
      '#type' => 'textarea',
      '#title' => t($title),
      '#default_value' => isset($flow_text_values[$field]) ? $flow_text_values[$field] : null,
    );
  }
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['#submit'][] = 'ae_social_login_data_business_rules_flow_text_form_submit';
  return $form;
}

function ae_social_login_data_business_rules_flow_text_form_submit($form, &$form_state) {
  $ae_settings = variable_get('ae_settings_master', array());
  $flow_text = isset($ae_settings['flow_text']) ? $ae_settings['flow_text'] : array();
  $values = $form_state['values'];

  foreach ($values as $form_field_name => $field) {
    if(stristr($form_field_name, "ae_") && $field){
      $key = str_replace('ae_flow_text_', '', $form_field_name); 
      $flow_text[$key] = check_url($field); 
    }
  }
  $ae_settings['flow_text'] = $flow_text;
  variable_set('ae_settings_master', $ae_settings);
}

function ae_social_login_data_business_rules_business_rules_form($form, &$form_state) {
  //## BUSINESS RULES

  $ae_settings = variable_get('ae_settings_master', array());

  $form['ae_enable_coppa_compliance'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable COPPA Compliance'),
    '#default_value' => isset($ae_settings['minimum_age']) ? 1 : 0,
    '#description' => t('US Law: DOB required and age >= 13'),
  );
  $form['ae_verify_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verify Email'),
    '#default_value' => isset($ae_settings['verify_email']) ? $ae_settings['verify_email'] : false,
    '#description' => t('With services where email is not automatically collected (eg. Twitter), ask the user to verify their email address'),
  );
  $form['ae_no_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('No Email'),
    '#default_value' => isset($ae_settings['no_email']) ? $ae_settings['no_email'] : false,
    '#description' => t('Turn off email sign up entirely'),
  );
  $form['ae_mobile_detect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Detect Mobile'),
    '#default_value' => isset($ae_settings['mobile_detect']) ? $ae_settings['mobile_detect'] : false,
    '#description' => t('Detect if sign in is being used from a mobile device'),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['#submit'][] = 'ae_social_login_data_business_rules_business_rules_form_submit';
  return $form;
}

function ae_social_login_data_business_rules_business_rules_form_submit($form, &$form_state) {
  $ae_settings = variable_get('ae_settings_master') ? variable_get('ae_settings_master') : array();
  foreach ($form as $index => $field) {
    $key = str_replace("ae_", "", $index);
    if(preg_match('/ae\_*/', $index) && is_array($field) && isset($field['#value']) && $field['#value'] && $field['#value'] !== 0){      
      if($field['#value'] == 1) $field['#value'] = true;
      $ae_settings[$key] = $field['#value'];
    } else {
      unset($ae_settings[$key]);
    }
  }
  if(isset($ae_settings['enable_coppa_compliance']) && $ae_settings['enable_coppa_compliance']){
    $ae_settings['minimum_age'] = 13;
  } else {
    unset($ae_settings['minimum_age']);
  }
  unset($ae_settings['enable_coppa_compliance']);
  variable_set('ae_settings_master', $ae_settings);
}

function ae_social_login_data_business_rules_field_mapping_form($form, &$form_state){
  //## Field Mapping
  $form['ae_field_mapping_main_description'] = array(
    '#markup' => t("<div class=\"description\"> Select Drupal user and <a target=_'blank' href='https://www.drupal.org/project/profile2'>profile</a> fields to update with values from the AE user on every login.</div>"),
    '#weight' => -10,
  );
  $ae_fields = AESettings::$extra_fields;
  $ae_field_mapping_options = array( 'none' => '- None -' );

  // Build select options for available mapping fields
  $user_fields = field_info_instances('user', 'user');
  foreach($user_fields as $field_name => $user_field){
    $ae_field_mapping_options["user.$field_name"] = "$field_name (user)";
  }

  if(module_exists("profile2")){
    foreach (profile2_get_types() as $profile_type_name => $profile_type) {
      $profile_fields = field_info_instances('profile2', $profile_type_name);
      foreach($profile_fields as $field_name => $profile_field){
        $ae_field_mapping_options["profile2.$profile_type_name.$field_name"] = "$field_name ($profile_type_name profile)";
      }
    }
  }

  $ae_field_mapping = variable_get('ae_field_mapping', array());
  //TODO this form should be reversed profile & user fields -> ae fields as you can technically map to both.
  $weight = 0;
  foreach($ae_fields as $field){
    if($field == 'email' || $field == 'password'){
      // email and password will be handled automatically.
      continue;
    }
    $form['ae_drupal_field_'.$field] = array(
      '#type' => 'select',
      '#title' => t('AE Field: '.$field),
      '#options' => $ae_field_mapping_options,
      '#default_value' => isset($ae_field_mapping[$field]) ? $ae_field_mapping[$field] : 'none',
      '#weight' => $weight++,
    );
    if($ae_field = 'avatarurl'){
      $form['ae_use_local_avatar'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use Local Avatar'),
        '#description' => t('Get the AE profile avatar from the local Drupal filesystem or from AE directly.'),
        '#default_value' => variable_get('ae_use_local_avatar', 1),
        '#weight' => $weight++,
      );
    }
  }

  $form['actions']['#weight'] = $weight++;
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Mappings'),
  );
  $form['#submit'][] = 'ae_social_login_data_business_rules_field_mapping_form_submit';
  return $form;
}

function ae_social_login_data_business_rules_field_mapping_form_submit($form, &$form_state){
  $field_mapping = array();
  foreach($form_state['values'] as $field_name => $value){
    if(strpos($field_name, 'ae_drupal_field_') === 0) {
      $ae_field = str_replace('ae_drupal_field_', '', $field_name);
      if($value !== 'none' ){
        $field_mapping[$ae_field] = $value;
      }else if(isset($field_mapping[$ae_field])){
        unset($field_mapping[$ae_field]);
      }
    }
  }
  variable_set('ae_field_mapping', $field_mapping);
  variable_set('ae_use_local_avatar', $form_state['values']['ae_use_local_avatar']);
}

/**
 *
 *
 * @param $form
 * @param $form_state
 */
function ae_social_login_forms_default_form_settings($form, &$form_state){

  $ae_social_login_forms = variable_get('ae_social_login_forms', array());
  $ae_social_login_data_ae_login = variable_get('ae_social_login_data_ae_login', 1);
  $ae_social_login_user_profile_register = variable_get('ae_social_login_user_profile_register', 0);

  $form['ae_social_login_forms'] = array(
    '#type' => 'fieldset',
    '#title' => t('Forms'),
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['ae_social_login_forms']['user_pass'] = array(
    '#type' => 'checkbox',
    '#title' => t('Attach to Password Reset Form'),
    '#description' => t('Send an AE password reset email when the <em>user_pass</em> form is submitted and no local user exists.'),
    '#default_value' => isset($ae_social_login_forms['user_pass']) ? $ae_social_login_forms['user_pass'] : 1,
  );

  $form['ae_social_login_forms']['user_login'] = array(
    '#type' => 'checkbox',
    '#title' => t('Attach to User Login Form'),
    '#description' => t('Add AE login to the <em>user_login</em> and <em>user_login_block</em> form instead of standard Drupal login .'),
    '#default_value' => isset($ae_social_login_forms['user_login']) ? $ae_social_login_forms['user_login'] : 1,
  );

  $form['ae_social_login_forms']['user_register_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Attach to User Register Form'),
    '#description' => t('Add AE login to the <em>user_register_form</em> form.'),
    '#default_value' => isset($ae_social_login_forms['user_register_form']) ? $ae_social_login_forms['user_register_form'] : 1,
  );

  $form['ae_social_login_data_ae_login'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disallow new registrations on login forms'),
    '#description' => t('By default AE forms allow registrations or logins. Enable to require new registrations through the registration form.<br><em>Note:</em> To register existing Drupal accounts with AE see <em>ae_social_login_form_ajax_validate</em> and remove <em>data-ae-type=login</em> via AJAX before form submission.'),
    '#default_value' => $ae_social_login_data_ae_login,
  );


  $form['ae_social_login_user_profile_register'] = array(
    '#type' => 'checkbox',
    '#title' => t('Register Account on User Profile Form'),
    '#description' => t('Register an Appreciation Engine account when the user adds a password to their account if one does not exist. This may have privacy implications, verify before implementing.'),
    '#default_value' => $ae_social_login_user_profile_register,
  );

  $form = system_settings_form($form);
  return $form;
}

//## Utilities
function ae_social_login_form_ae_social_login_basic_settings_form_alter(&$form, &$form_state, $form_id) {
  $verb = variable_get('ae_api_key') ? "Refresh" : "Load";
  $form['actions']['submit']['#value'] = $verb.' AE Application Info';
}

function _ae_social_login_get_title_from_field($field){
  $title_arr = preg_split('/\_/', $field);
  $title = '';
  foreach($title_arr as $val){
    $title .= ucfirst($val)." ";
  }
  return trim($title);
}
/* globals Drupal, jQuery */
(function ($) {
	"use strict";
	Drupal.behaviors.AEWebformAdmin = {
		attach: function (context) {
			$("#edit-extra-ae-services").sortable();
		}
	};
})(jQuery);

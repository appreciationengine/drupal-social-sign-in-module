/*globals jQuery, Drupal, Modernizr, aeJS */
//# AE Webform
(function($){
	"use strict";
	var dbae = Drupal.behaviors.ae_social_login,
		dbaw = Drupal.behaviors.ae_webform = {
		//## Drupal Behaviors Attach
		attach: function(context, settings){
			var $context = $(context),
				$components = $context.find('.webform-component-ae-login'),
				$forms = $components.closest('form.webform-client-form');

			$forms.once("ae-webform-attached", function(){
				// Note that multiple ae_login components are not supported.
				var $form = $(this),
					$component = $form.find(".webform-component-ae-login").first();
				dbaw.init($form, $component);

			});
		},
		//## Initialize ae_webform
		init: function($form, $component){
			if(!dbae.isReady){ return dbae.onReady(function(){ dbaw.init($form, $component); }); }
			// Once AE is ready...
			var $ae_user = $component.find("input[name='ae_user']").first(),
				$ae_uid = $component.find("input[data-ae-uid]").first(),
				nid = $form.attr("data-nid"),
				key = $ae_uid.attr("name").replace("submitted[", "").replace("]", ""),
				settings = Drupal.settings.ae_webform["nid_"+nid][key];
			if(aeJS.user.data){
				dbaw.saveUser($ae_user, $ae_uid, aeJS.user);
				//### If connected,
				var connected = dbaw.validateAEUser($form, aeJS.user, settings);
				if(connected && settings.autosubmit && !settings.hideSubmit){
					// then show the form submit button instead of the login button.
					dbaw.showSubmit($form);
				}
			}

			function onLogin(ae_user){
				//### On AE login, save the ae_user to the form & update form fields.
				dbaw.saveUser($ae_user, $ae_uid, ae_user);
				if(settings.autofill){
					dbaw.autofill($form, ae_user);
				}
				if(settings.autosubmit){
					// Autosubmit the form on login
					var valid = true, ae_user_valid = dbaw.validateAEUser($form, ae_user, settings);
					$form.find("*[required]:not(div,fieldset)").each(function(){
						if($(this).val() === ""){ valid = false; }
					});
					if(valid && ae_user_valid){
						$form.submit();
					}else if(ae_user_valid){
						dbaw.showSubmit($form);
					}
				}
			}
			
			// ### Preserve form values on mobile redirect
			function preserveForm(){
				if(!dbae.isMobile){ return; }
				try{
					var state = { formId: $form.attr("id")};
					$form.find("input,select").each(function(){
						var $field = $(this), fieldId = this.id;
						if(!fieldId ||fieldId === ""){ return; }
						if($field.is("input[type=checkbox]")){
							state[fieldId] = $field.is(":checked");
						}else{
							state[fieldId] = $field.val();
						}
					});
					
					if(window.sessionStorage){
						sessionStorage.setItem("ae_webform."+state.formId,JSON.stringify(state));
					}
				}catch(e){}
			}
			
			function restoreForm(){
				try{
					var formId = $form.attr("id"),
						state = sessionStorage.getItem("ae_webform."+formId);
					if(!state){ return; }
					sessionStorage.removeItem("ae_webform."+formId);
					state = JSON.parse(state);
					
					$form.find("input,select").each(function(){
						var $field = $(this), fieldId = this.id;
						if(fieldId && fieldId !== "" && state[fieldId] !== undefined){
							if($field.is("input[type=checkbox]")){
								$field.prop("checked", state[fieldId]);
							}else{
								$field.val(state[fieldId]);
							}
						}
					});
				}catch(e){}
			}

			$form.find("a.ae-register-link").on("click", preserveForm);
			aeJS.events.onMobileDetect.addHandler(restoreForm);
			if(dbae.isMobile){ restoreForm(); }
						
			$form.find("a.ae-register-link").on("ae_login", function(e, $el, ae_user){
				setTimeout(function(){onLogin(ae_user);}, 0);
			});

			aeJS.events.onLogout.addHandler(function(){
				//### On logout, clear the ae_user from the form.
				dbaw.clearUser($ae_user, $ae_uid);
				$form.find(".ae-social-login-links a").removeClass("connected");
			});

			$form.find(".ae-webform-submit-wrapper").on("click", function(e){
				var input = $(this).find("input")[0];
				if(e.target !== input){
					e.stopImmediatePropagation();
					$(input).click();
				}
			});

		},
		saveUser: function($ae_user, $ae_uid, ae_user){
			$ae_user.val(JSON.stringify(ae_user));
			$ae_uid.val(ae_user.data.ID);
		},
		clearUser: function($ae_user, $ae_uid){
			$ae_user.val(null);
			$ae_uid.val(null);
		},
		//## Validate ae_user services
		validateAEUser: function($form, ae_user, settings){
			if(!ae_user){ return false; }
			var connected = false, connected_services = 0, service;
			for(var s in ae_user.services){
				service = ae_user.services[s].Service;
				if(settings.services.indexOf(service) >= 0){
					connected = true;
					connected_services++;
				}
			}

			if(settings.required_services === 1){
				return connected_services === settings.services.length;
			}else{
				return connected;
			}
		},
		showSubmit: function($form){
			$form.find(".form-submit, .ae-webform-submit-wrapper").show();
			$form.find(".ae-social-login-links").hide();
			$form.find(".form-actions").removeClass("hide-submit");
			return this;
		},
		hideLogin: function($form, social){
			$form.find(".ae-social-login-links, .webform-component-ae-login-description").hide();
			return this;
		},
		//## Autofill
		autofill: function($form, ae_user){
			var mappings = {
				"email": "Email",
				"given-name": "FirstName",
				"family-name": "LastName",
				"bday": "BirthDate",
				"bday-year": "BirthDate",
				"postal-code": "PostCode",
				"address-level1": "City",
				"address-level2": "State",
				"country": "CountryCode",
				"country-name": "Country"
			};

			//### Map ae_user fields to autocomplete fields
			var aeField, $field, fieldValue, aeValue;
			for(var autocomplete in mappings){
				aeField = mappings[autocomplete];
				aeValue = ae_user.data[aeField];
				if(aeValue){
					$field = $form.find("*[autocomplete='"+autocomplete+"']");
					if($field.length === 0){ continue; }
					fieldValue = $field.val();
					if(autocomplete === "bday-year" && fieldValue === ""){
						var $select = $field.closest(".webform-component-date "),
							$day = $select.find("select.day"),
							$month = $select.find("select.month"),
							$year = $select.find("select.year, input.year"),
							dob = aeValue.split("-");
						if(dob.length === 3){
							$year.val(dob[0]);
							$month.val(dob[1].replace(/^0/,''));
							$day.val(dob[2].replace(/^0/,''));
						}
					}else if(fieldValue === "" && fieldValue !== aeValue){
						$field.val(aeValue).trigger("change");
					}
				}
			}
		}
	};
})(jQuery);

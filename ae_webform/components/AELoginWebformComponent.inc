<?php
namespace Appreciation_Engine\Webform;
use Appreciation_Engine\AESettings;

class AELoginWebformComponent{

  // ## Component Settings Form
  /**
   * Create the ae_login webform component settings form.
   *
   * @param array $form
   * @param       $defaults
   * @return array
   */
  public static function component_settings(&$form = [], $defaults){
    $weight = 0;
    $services = AESettings::services_options(FALSE);
    if(isset($services['email'])) {
      // Don't allow email login on webforms.
      unset($services['email']);
    }
    // Sort the options in the order of the settings. settings can be re-ordered
    // in the UI via drag and drop & jquery.sortable.
    $services = array_merge($defaults['ae_services'], $services);
    // ### AE Service options
    $form['ae_services'] = [
      '#type' => 'checkboxes',
      '#title' => t('Social Services'),
      '#description' => t('Select which social services to expose for login. Drag and drop to re-order services.'),
      '#options' => $services,
      '#default_value' => $defaults['ae_services'],
      '#required' => TRUE,
      '#weight' => $weight++,
    ];

    // ### Required Services
    $form['required_services'] = [
      '#type' => 'checkbox',
      '#title' => t('Require All Services'),
      '#default_value' => $defaults['required_services'],
      '#description' => t('Select if all services must be connected to submit the form.'),
      '#weight' => $weight++,
    ];

    // ### Autofill
    $form['autofill'] = [
      '#type' => 'checkbox',
      '#title' => t('Autofill'),
      '#default_value' => $defaults['autofill'],
      '#description' => t('Autofill form fields with AE user data on login based on autofill attributes.'),
      '#weight' => $weight++,
    ];

    // ### Local Site Login
    $form['site_login'] = [
      '#type' => 'checkbox',
      '#title' => t('Site Login'),
      '#default_value' => $defaults['site_login'],
      '#description' => t('Login or Register a local website account for the user upon submission. Note that this has a performance impact and is not required.'),
      '#weight' => $weight++,
    ];

    // ### Autosubmit
    $form['autosubmit'] = [
      '#type' => 'checkbox',
      '#title' => t('Autosubmit'),
      '#default_value' => $defaults['autosubmit'],
      '#description' => t('Submit the form as soon as the social login completes if the form validates client-side.'),
      '#weight' => $weight++,
    ];

    // Add the component form validation callback. While an element_validate
    // callback this will actually validate the entire form.
    $form['ae_services']['#element_validate'] = ['ae_webform_ae_login_component_settings_validate'];

    drupal_add_library('ae_webform', 'ae_webform.admin');
    return $form;
  }

  //### Component Settings Form Validation
  /**
   * Validate the ae_login component settings form.
   * @param $form
   * @param $form_state
   */
  public static function component_settings_validate($form, &$form_state){
    // Don't allow a form_key of ae_user as it conflicts with the ae_user hidden input.
    $form_key = $form_state['values']['form_key'];
    if($form_key == 'ae_user'){
      form_set_error('form_key', t('Please select a different field key `ae_user` is a reserved key.'));
    }
  }

  // ## Render Component Form Element
  /**
   * @param      $component
   * @param null $value
   * @param bool $filter
   * @param null $submission
   * @return array
   */
  public static function render($component, $value = NULL, $filter = TRUE, $submission = NULL){
    $nid = $component['nid'];
    $node = isset($nid) ? node_load($nid) : NULL;
    $form_key = $component['form_key'];
    $element = [
      '#type' => 'container',
      '#tree' => FALSE,
      '#attributes' => [ 'class' => [ 'webform-component', 'webform-component-ae-login' ], ],
      '#element_validate'  => [ 'ae_webform_ae_login_component_validate' ],
    ];

    // ### Create Social Login Buttons
    $social_buttons = [
      '#type' => 'fieldset',
      '#tree' => FALSE,
      '#title' => $filter ? webform_filter_xss($component['name']) : $component['name'],
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => [ 'class' => [ 'ae-social-login-links' ], ],
    ];
    foreach($component['extra']['ae_services'] as $service => $enabled){
      if($service === $enabled){
        $social_buttons[$service] = [
          '#theme' => 'ae_register_link',
          '#social' => $service,
          '#remote_only' => TRUE,
        ];
      }
    }

    // Add the field title & description.
    $title_display = $component['extra']['title_display'] ? $component['extra']['title_display'] : 'before';
    if($title_display == "none"){
      unset($social_buttons['#title']);
    }else{
      $social_buttons['#title_display'] = $title_display;
    }

    $description = $filter ? webform_filter_descriptions($component['extra']['description'], $node) : $component['extra']['description'];
    if(!empty($description)){
      if($component['extra']['description_above'] == 1){
        $social_buttons['#description'] = $description;
      }else{
        $social_buttons['#suffix'] = "<div class=\"fieldset-description webform-component-ae-login-description\">$description</div>";
      }
    }

    // ### Create hidden inputs.
    $ae_user = [
      '#type' => 'hidden',
      '#default_value' => '',
    ];
    $ae_uid = [
      '#type' => 'hidden',
      '#default_value' => $value,
      '#attributes' => [ 'data-ae-uid' => NULL ],
      '#name' => "submitted[{$form_key}]",
      '#parents' => [ 'submitted', $form_key ],
      '#array_parents' => [ 'submitted', $form_key ],
      '#webform_component' => $component,
    ];

    // Add the form fields to the element.
    $element['socials'] = $social_buttons;
    $element['ae_user'] = $ae_user;
    $element[$form_key] = $ae_uid; // Note that the uid is added with the $form_key.

    // Add JS settings indexed by node & $form_key.
    $component_js_settings = [
      'ae_webform' => [
        "nid_$nid" => [
          $form_key => [
            'services' => array_keys(array_filter($component['extra']['ae_services'])),
            'required_services' => $component['extra']['required_services'],
            'autofill' => $component['extra']['autofill'],
            'site_login' => $component['extra']['site_login'],
            'autosubmit' => $component['extra']['autosubmit'],
          ],
        ]
      ]
    ];
    drupal_add_js($component_js_settings, 'setting');

    drupal_add_library('ae_webform', 'ae_webform.component');
    return $element;
  }

  // ## Validate ae_login Component
  /**
   * @param $element
   * @param $form_state
   * @return bool
   */
  public static function element_validate($element, &$form_state){
    $component = $element['#webform_component'];
    $ae_services = array_values(array_filter($component['extra']['ae_services']));
    $required_services = $component['extra']['required_services'];
    $form_key = $component['form_key'];

    $ae_user = empty($form_state['values']['ae_user']) ? NULL : json_decode($form_state['values']['ae_user']);
    $ae_uid = isset($form_state['values']['submitted'][$form_key]) ? $form_state['values']['submitted'][$form_key] : NULL;

    if($component['required'] == 1){
      // ### Verify that the ae_user contains one of the services.
      $missing_services = TRUE;
      $matching_services = [];
      if (isset($ae_user) && isset($ae_user->services)){
        foreach($ae_user->services as $s){
          if(in_array($s->Service, $ae_services)){
            $matching_services[] = $s->Service;
          }
        }
      }

      if($required_services === 1 && count($matching_services) == count($ae_services)){
        // If all services must be connected
        $missing_services = FALSE;
      }else if($required_services === 0 && !empty($matching_services)){
        // otherwise accept any service.
        $missing_services = FALSE;
      }

      if(empty($ae_uid)){ $missing_services = TRUE; }

      if($missing_services){
        $services = implode(', ', array_map('ucwords', $ae_services));
        $pos = strrpos($services, ',');
        $conj = $required_services === 1 ? ' and' : ' or';
        if($pos !== FALSE){
          $services = substr_replace($services, $conj, $pos, 1);
        }
        form_set_error($form_key, t('Please login with @services to submit.', [ '@services' => $services ]));
        return FALSE;
      }
    }

    // Set the ae_user in $form_state for use in form_submit.
    if(!empty($ae_user)){
      $form_state['ae_user'] = $ae_user;
    }
  }

  /**
   * Placeholder for element_submit to get field value, ae_userid.
   * @param $component
   * @param $value
   * @return mixed
   */
  public static function element_submit($component, $value){
    return $value;
  }

  /**
   * Display the ae_user ID in the detailed report.
   * @param        $component
   * @param        $value
   * @param string $format
   * @param array  $submission
   * @return array
   */
  public static function display_reporting($component, $value, $format = 'html', $submission = []){
    $element = [
      '#title' => t('Appreciation Engine User ID'),
      '#title_display' => 'before',
      '#weight' => $component['weight'],
      '#theme' => 'webform_display_textarea',
      '#theme_wrappers' => $format == 'html' ? ['webform_element'] : ['webform_element_text'],
      '#format' => $format,
      '#value' => isset($value[0]) ? $value[0] : '',
      '#translatable' => ['title'],
    ];
    return $element;
  }

  /**
   * Display ae_user ID in the table report.
   * @param $component
   * @param $value
   * @return string
   */
  public static function display_table($component, $value){
    return check_plain(empty($value[0]) ? '' : $value[0]);
  }

}

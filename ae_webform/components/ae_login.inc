<?php
/**
 * This file contains webform API specific hooks for the implementation of our
 * ae_login webform component. The logic for these functions resides in our
 * AELoginWebformComponent class for future ease of use in Drupal 8 and code
 * organization.
 */

// ## Component Business Logic
module_load_include('inc', 'ae_webform', 'components/AELoginWebformComponent');
use Appreciation_Engine\Webform\AELoginWebformComponent;

// ## Webform Component Interfaces
/**
 * Component element form validation callback.
 * @param $element
 * @param $form_state
 * @return bool
 */
function ae_webform_ae_login_component_validate($element, &$form_state){
  return AELoginWebformComponent::element_validate($element, $form_state);
}

/**
 * Component settings form validation callback.
 * Note that this is technically implemented as an element_validate hook,
 * because form validation isn't exposed via webform API.
 * @param $element
 * @param $form_state
 */
function ae_webform_ae_login_component_settings_validate($element, &$form_state){
  $form = $form_state['complete form'];
  AELoginWebformComponent::component_settings_validate($form, $form_state);
}

// ## Webform Component Definitions
/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_ae_login() {
  return [
    'name' => '',
    'form_key' => NULL,
    'required' => 0,
    'pid' => 0,
    'weight' => 0,
    'extra' => [
      'ae_services' => [],
      'required_services' => 0,
      'autofill' => 1,
      'site_login' => 0,
      'autosubmit' => 0,
      'description_above' => FALSE,
      'private' => FALSE,
      'analysis' => FALSE,
    ],
  ];
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_ae_login($component) {
  $form = [ 'extra' => [] ];
  AELoginWebformComponent::component_settings($form['extra'], $component['extra']);
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_ae_login($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  return AELoginWebformComponent::render($component, $value, $filter, $submission);
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_ae_login($component, $value, $format = 'html', $submission = []) {
  return AELoginWebformComponent::display_reporting($component, $value, $format, $submission);
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_ae_login($component, $value) {
  return AELoginWebformComponent::display_table($component, $value);
}

/**
 * Implements _webform_submit_component().
 */
function _webform_submit_ae_login($component, $value) {
  return AELoginWebformComponent::element_submit($component, $value);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_ae_login($component, $export_options) {
  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $export_options['header_keys'] ? $component['form_key'] : $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_ae_login($component, $export_options, $value) {
  return empty($value[0]) ? '' : $value[0];
}


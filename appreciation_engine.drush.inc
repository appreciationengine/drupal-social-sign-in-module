<?php
function appreciation_engine_drush_command() {
  $items = array();
  // Name of the drush command.
  $items['appreciation_engine_apikey'] = array(
    'description' => 'Set the AE API Key.',
    'callback' => 'appreciation_engine_set_api_key',
    'arguments' => array(
      'apikey' => 'The application api key.',
    ),
    'aliases' => array('ae_apikey'),
    //'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL,
  );
  $items['appreciation_engine_appinfo'] = array(
    'description' => 'Update AE Application info.',
    'callback' => 'appreciation_engine_get_app_info',
    'options' => array(
      'apikey' => 'The application api key.',
      'domain' => 'The application domain.',
    ),
    'aliases' => array('ae_appinfo'),
    //'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL,
  );
  return $items;
}